# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import os
import re
import sys
import dep
import base64
import readline
import traceback

import dbg
from info import *
from object import *
from eval import *
from stdsyms import *
from macro import *

loadpath = ['.']
suffixes = ['', '.l', str(base64.b64decode('Lmxpc3A='))]

def loadFile(fname, missing_ok=False):
    try:
        # dbg.p(dbg.load, "try to load", fname)
        with open(str(fname)) as f:
            info("; loading", fname)
            dep.repl(f, fname)
        return T
    except FileNotFoundError as e:
        if missing_ok:
            return Nil
        raise PyleOSError(e, fname)
    except OSError as e:
        raise PyleOSError(e, fname)

def trySuffixes(fname):
    for suffix in suffixes:
        if loadFile(fname + suffix, True):
            return T
    return Nil

def load(name, missing_ok=False):
    if re.search("/", name):
        found = trySuffixes(name)
        if found:
            return T
        if missing_ok:
            return Nil
        raise PyleFileNotFoundError(str(name), load=True)
    for dir in loadpath:
        result = load(os.path.join(dir, name), True)
        if result:
            return T
    if missing_ok:
        return Nil
    raise PyleFileNotFoundError(str(name), load=True)

def is_interactive(in_port, out_port):
    return in_port.isatty() and out_port and out_port.isatty()

def repl(inp, fname, out=None):
    reader = Reader(inp, fname, out)
    # dbg.p(dbg.repl, "start repl on", fname)
    try:
        while True:
            if reader.interactive:
                # We set the prompt at the start of reading an expression; it is
                # emptied after the first line input(), and restored for the
                # text expression here.
                reader.prompt = "> "
            ob = reader.read()
            if ob is not None:
                # dbg.p(dbg.repl, "=> {o} ({t})".format(o=ob, t=type(ob)))
                try:
                    expr = macroexpandForm(ob)
                    # dbg.p(dbg.repl, "exp {e} ({t})".
                    #       format(e=expr, t=type(expr)))
                    value = Eval(expr)
                    if out:
                        print("\n" + repr(value), file=out)
                    else:
                        # dbg.p(dbg.load, "value:", value)
                        pass
                except PyleThrow as e:
                    print("\nERR throw without catch, tag: {t}; value: {v}".
                          format(t=repr(e.tag()), v=repr(e.value())))
                    if dep.exit_on_error:
                        sys.exit("exit due to error")
                except PyleError as e:
                    if EnableTracebackSymbol.value():
                        tb = sys.exc_info()[2]
                        traceback.print_tb(tb)
                    print(e)
                    if dep.exit_on_error:
                        sys.exit("exit due to error")
            else:
                if reader.interactive:
                    print(file=out)
                return
    finally:
        reader.close()


class Token():
    def __init__(self):
        self.classname = self.__class__.__name__
    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        return "T<{n}:{i}>".format(n=self.classname, i=id(self))
    pass
class OparenToken(Token):
    pass
class CparenToken(Token):
    pass
class QuoteToken(Token):
    pass
class DotToken(Token):
    pass
class CommentToken(Token):
    pass
class FunctionToken(Token):
    #'
    pass
class QuasiquoteToken(Token):
    pass
class UnquoteToken(Token):
    pass
class UnquoteSplicingToken(Token):
    pass
class AtomToken(Token):
    def __repr__(self):
        return "T<{n} [{b}] {k}>".\
          format(n=self.classname, b=self.buf,
                     k="String" if self.isstring else "Symbol/Number")
    def __init__(self, init="", isstring=False):
        super().__init__()
        self.buf = init
        self.isstring = isstring
    def add(self, ch):
        self.buf += ch
    def getAtom(self):
        if self.isstring:
            return String(self.buf)
        try:
            try:
                return Number(int(self.buf))
            except:
                return Number(float(self.buf))
        except:
            return intern(self.buf)
class EOFToken(Token):
    pass

escapeSpecials = { 'a': '\a', 'b': '\b', 'f': '\f', 'n': '\n',
                       'r': '\r', 't': '\t', 'v': '\v', '"': '"', '\\': '\\'  }
#' and (currently no) other tokens of that form
octothorpeTokens = { "'": FunctionToken(), }

# a number of tokens that produce (sym arg) constructs
readerMacros = {
    FunctionToken: FunctionSymbol,
    QuasiquoteToken: QuasiquoteSymbol,
    QuoteToken: QuoteSymbol,
    UnquoteToken: UnquoteSymbol,
    UnquoteSplicingToken: UnquoteSplicingSymbol,
}

class StringPort():
    def __init__(self, string):
        self.string = string
        self.position = 0
        self.length = len(string)
    def read(self, n):
        left = self.length - self.position
        if n > left:
            n = left
        newpos = self.position + n
        substr = self.string[self.position:newpos]
        self.position = newpos
        return substr
    def readline(self):
        result = ""
        while True:
            ch = self.read(1)
            if not ch:
                return result
            result += ch
            if ch == "\n":
                return result
    def isatty(self):
        return False

class Reader():
    def __init__(self, inp, fname, out):
        self.inp = inp
        self.fname = fname
        self.unreadchar = None
        self.pushbacktoken = None
        self.linenumber = 0
        self.currentLine = None
        self.lineLen = 0
        self.linePos = 0
        self.interactive = is_interactive(inp, out)
        self.prompt = ""
    def close(self):
        pass
    def unread(self, ch):
        self.unreadchar = ch
    def pushback(self, token):
        assert self.pushbacktoken is None
        self.pushbacktoken = token
        dbg.p(dbg.read, "push back token", token)
    def getline(self):
        "Get a new line; return truish iff there was one."
        if self.interactive:
            try:
                line = input(self.prompt)
                # Prompt shall only be set at the beginning of an expression,
                # for which it is said before the repl calls the read() method.
                self.prompt = ""
            except EOFError:
                return False
            line += "\n"
        else:
            line = self.inp.readline()
        self.currentLine = line
        self.linePos = 0
        self.lineLen = len(line)
        return line
    def getchar(self):
        if self.linePos >= self.lineLen:
            hadOne = self.getline()
            if not hadOne:
                return ""
        ch = self.currentLine[self.linePos]
        self.linePos += 1
        return ch
    def nextChar(self, inString=False):
        if self.unreadchar:
            ch = self.unreadchar
            self.unreadchar = None
            return ch
        else:
            inComment = False
            while True:
                ch = self.getchar()
                if ch == "\n":
                    self.linenumber += 1
                if ch == ";" and not inString:
                    inComment = True
                    continue
                elif inComment:
                    if ch == "\n":
                        inComment = False
                    continue
                else:
                    return ch

    def nextNonSpace(self):
        while True:
            ch = self.nextChar()
            if not ch or not ch.isspace():
                return ch
    def nextToken(self):
        if self.pushbacktoken:
            t = self.pushbacktoken
            self.pushbacktoken = None
            result = t
        else:
            ch = self.nextNonSpace()
            if not ch:
                result = EOFToken()
            elif ch == "(":
                result = OparenToken()
            elif ch == ")":
                result = CparenToken()
            elif ch == ".":
                result = self.dotOrAtomToken()
            elif ch == '"':
                result = self.collectString()
            elif ch == "'":
                result = QuoteToken()
            elif ch == "#":
                result = self.octothorpeToken()
            elif ch == ",":
                result = self.unquoteOrUnquoteSplicingToken()
            elif ch == "`":
                result = QuasiquoteToken()
            else:
                result = self.collectAtom(ch)
        # dbg.p(dbg.read, "next token is", result)
        return result
    def unquoteOrUnquoteSplicingToken(self):
        ch = self.nextChar()
        if ch == "@":
            return UnquoteSplicingToken()
        else:
            self.unread(ch)
            return UnquoteToken()
    def octothorpeToken(self):
        ch = self.nextChar()
        t = octothorpeTokens.get(ch)
        if t:
            return t
        raise PyleSyntaxError("no octothorpe token defined with '{ch}'".
                                  format(ch=ch), self.linenumber, self.fname)
    def dotOrAtomToken(self):
        ch = self.nextChar()
        if isDelimiter(ch):
            self.unread(ch)
            return DotToken()
        return self.collectAtom("." + ch)
    def collectAtom(self, init):
        atom = AtomToken(init)
        while True:
            ch = self.nextChar()
            if isDelimiter(ch):
                self.unread(ch)
                return atom
            atom.add(ch)
    def collectString(self):
        string = AtomToken(isstring=True)
        inEscape = False                  # after backslash
        while True:
            ch = self.nextChar(inString=True)
            if inEscape:
                inEscape = False
                ch = escapeSpecials.get(ch, ch)
            elif ch == "\\":
                inEscape = True
                continue
            elif ch == '"':
                return string
            string.add(ch)
    def read(self):
        t = self.nextToken()
        ttype = type(t)
        if ttype is EOFToken:
            return None
        if ttype is AtomToken:
            return t.getAtom()
        if ttype is OparenToken:
            return self.collectList()
        if ttype in readerMacros:
            return list(readerMacros[ttype], self.read())
        if ttype is DotToken:
            raise PyleSyntaxError('unexpected "." found', self.linenumber,
                                  self.fname)
        if ttype is CparenToken:
            raise PyleSyntaxError("unexpected closing paren found",
                                  self.linenumber, self.fname)
        assert False, "unhandled token {t} found".format(t=t)
    def collectList(self):
        lc = ListCollector()
        while True:
            t = self.nextToken()
            # dbg.p(dbg.read, "collectList sees", t)
            ttype = type(t)
            if ttype is CparenToken:
                # dbg.p(dbg.read, "collectList done with", lc.list())
                return lc.list()
            if ttype is DotToken:
                end = self.read()
                cparen = self.nextToken()
                if type(cparen) is not CparenToken:
                    raise PyleSyntaxError('unexpected second token'
                                              ' after ".": ' + str(cparen),
                                              self.linenumber, self.fname)
                lc.addEnd(end)
                return lc.list()
            if ttype is EOFToken:
                raise PyleSyntaxError("unexpected EOF in List",
                                          self.linenumber, self.fname)
            if ttype is AtomToken:
                lc.add(t.getAtom())
            else:
                self.pushback(t)
                lc.add(self.read())

def isDelimiter(ch):
    return ch.isspace() or ch in "()'\";"

dep.repl = repl
dep.load = load
