# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import re
import sys

eval = 1
env = 2
call = 3
var = 4
repl = 5
read = 6
env = 7
sym = 8
lc = 9                          # ListCollector
bi = 10                         # builtins
main = 11
let = 12
form = 13
load = 14
app = 15
mac = 16                        # macros
bind = 17                       # parameter binding

want_debug = set()

def reset():
    global want_debug
    want_debug = set()

def addFlag(flagname):
    myattrs = globals()
    if flagname in myattrs:
        want_debug.add(myattrs[flagname])
    else:
        print('ERR debug flag "{0}" unknown'.format(flagname), file=sys.stderr)

def getFlags():
    flags = []
    myattrs = globals()
    for flag in want_debug:
        for name in myattrs:
            if myattrs[name] == flag:
                flags.append(name)
    return flags
    
try:
    with open("py-debug.conf") as conf:
        for line in conf:
            if re.match(r"^\s*(#.*)?$", line):
                continue
            line = line.strip()
            addFlag(line)
except FileNotFoundError:
    pass


# return true iff it has printed something
def p(what, *args):
    if what in want_debug:
        args = map(str, args)
        print(*args)
        return True
    return False
