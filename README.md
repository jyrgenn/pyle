Python Lambda Experiment
========================

This is "pyle", an interpreter for s-expressions and lambda
functions written in Python.

Copyright (C) 2017, 2018 Juergen Nickelsen <ni@w21.org>. See LICENSE
for the conditions under which this code is licensed to you.

Pyle is meant to be a rather (although not strictly) minimal
interpreter, with a few features in the builtin core. More functionality
shall, if possible, be implemented in the language itself. In order
to do that, it has macros.

The supported data types are symbols, strings, numbers, pairs, and
errors. Variable numbers of arguments to functions are implemented
using improper argument lists like in Scheme. This, together with
the traditional defun syntax, means, consequently, that a function
of zero or more arguments will not have a parameter list, but rather
a parameter symbol, like this:

```
  (defun adder args
    (apply '+ args))
```
I admit this is more elegant in Scheme with `(define (adder . args) ...)`

**Update 2018-09**: Like similar interpreters before, I wrote Pyle
to learn more about its implementation language. I started it short
after starting to learn Python at all, and working on Pyle helped me
with that.

In between, as I have gained more experience with Python in other
projects, I would certainly do quite a few things in Pyle
differently now, e.g. register the builtin functions using a
decorator, and make the Object hierarchy somewhat less messy. I
don't know if I'll ever get around to doing that, but there are
certainly quite a few places where I could do things more elegant
and more pythonic.
