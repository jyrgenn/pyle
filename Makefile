# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

REMOTES = $$(git remote | sort -r)
JUNK = *~ *.pyc __pycache__ TAGS

default:
	@echo
	@echo "   This Makefile has no meaningful default target."
	@echo "   Try test, tags, clean, push."
	@echo

test:
	./pyle -se tests.l < data.l

tags: *.py
	etags *.py *.l

clean:
	rm -rf $(JUNK)

push:
	for r in $(REMOTES); do git push --all $$r; git push --tags $$r; done
