# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import dep

class PyleThrow(Exception):
    """This is the exception raised for a `throw'."""
    def __init__(self, tag, value):
        self._tag = tag
        self._value = value
    def __str__(self):
        return "#<{s} t:{t} v:{v}>".\
            format(s=self.__class__.__name__, t=self._tag, v=self._value)
    def value(self):
        return self._value
    def tag(self):
        return self._tag

class PyleError(Exception):     # virtual
    def __new__(cls, *args):
        newob = super().__new__(cls, *args)
        dep.makeError(newob)
        return newob
    def __str__(self):
        return "ERR " + self.message

class PyleUserError(PyleError):
    """This is raised by User code calling the `error' function."""
    def __init__(self, formatstring, *args):
        self.args = args
        self.message = formatstring.format(*args)

class PyleArgTypeError(PyleError):
    template = "{w} argument {o} [{t}] is not a {s}"
    def __init__(self, ob, what, shouldbe):
        self.ob = ob
        self.what = what
        self.shouldbe = shouldbe
        self.message = self.template.format(w=what, o=ob,
                                            t=ob.__class__.__name__, s=shouldbe)

class PyleModifyImmutableError(PyleError):
    def __init__(self, symbol):
        self.symbol = symbol
        self.message = "trying to modify immutable value of symbol {s}".\
          format(s=symbol)

class PyleUnboundVarError(PyleError):
    def __init__(self, symbol):
        self.symbol = symbol
        self.message = "trying to get value of unbound symbol {s}".\
          format(s=symbol)

class PyleArgCountError(PyleError):
    def __init__(self, function, have, should):
        self.function = function
        self.have = have
        self.should = should
        if have < should:
            problem = "few"
            demand = "needs at least"
        else:
            problem = "many"
            demand = "takes at most"
        self.message = "too {p} arguments ({h}) for {f}, {d} {s}".\
          format(p=problem, h=have, f=function, d=demand, s=should)

class PyleNoFunctionError(PyleError):
    def __init__(self, ob):
        self.ob = ob
        if ob.isSymbol:
            typ = "no function bound to symbol"
        else:
            typ = "not a function"
        self.message = "{t}: {o}".format(o=ob, t=typ)

class PyleFileOpenError(PyleError):
    def __init__(self, fname, load=False):
        self.fname = fname
        self.load = load
        loadS = "load " if load else ""
        self.message = "cannot find {l}file {f}".format(l=loadS, f=fname)

class PyleSyntaxError(PyleError):
    def __init__(self, errs, line, fname):
        self.errs = errs
        self.line = line
        self.fname = fname
        self.message = "syntax error: {f}:{l}: {e}".\
                       format(e=errs, l=line, f=fname)

class PyleIterateTypeError(PyleError):
    def __init__(self, ob):
        self.ob = ob
        self.message = "making iterator for invalid type {t}: {o}".\
                       format(t=ob.__class__.__name__, o=ob)

class PyleIterateAtomError(PyleError):
    def __init__(self, ob):
        self.ob = ob
        self.message = "try to iterate over atom or improper list: {o}".\
                       format(o=ob)

class PyleNotImplementedError(PyleError):
    def __init__(self, ob, method):
        self.ob = ob
        self.method = method
        self.message = "method {m} not implemented for {o}: {t}".\
                       format(m=method, o=ob, t=ob.__class__.__name__)

class PyleOSError(PyleError):
    def __init__(self, e, *args):
        self.message = e.strerror
        self.args = args
        if args:
            self.message += ": " + " ".join(map(str, args))
            
class PyleFileNotFoundError(PyleError):
    def __init__(self, fname, load):
        self.fname = fname
        self.load = load
        if load:
            loadstr = "load "
        else:
            loadstr = ""
        self.message = "{l}file {f} not found".format(l=loadstr, f=fname)

class PyleUnquoteError(PyleError):
    def __init__(self, symbol):
        self.symbol = symbol
        self.message = "{s} used outside of a quasiquote context".\
          format(s=symbol)

class PyleMacroCallError(PyleError):
    def __init__(self, name):
        self.name = name
        self.message = "macro {n} called directly (shouldn't happen)".\
          format(n=name)
