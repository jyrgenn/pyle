# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

verbosity = 0

def info(*args):
    if verbosity >= 1:
        print(*list(map(str, args)))
