# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

"""Pyle Builtin Functions.

All builtin functions are defined here with names "Pyle_*".

If the first line of the docstring starts with "@desc ", it contains
attributes of the function. It consists of a number of (all
optional) "keyword: value" pairs, separated by semicolons (with
optional following whitespace). These keywords are recognised:

  name:     name of the function (default; the part after the "Pyle_")
  args:     list of arguments, space-separated; args in brackets are
            optional, and an arg name ending in "..." denotes an arbitrary
            number (incl. zero) of following args; these are used to
            calculate the minimum and maximum number of arguments and to
            build the function synopsis (default: no args)
  special:  if truish, builtin is a special form (default: false)
  result:   a brief word for or description of the result value (default: "")

The rest of the docstring is the actual docstring of the builtin
function.

"""

# Python modules
import re
import sys
import copy
import inspect

# Pyle modules
import dbg
from utils import *
from object import *
from stdsyms import *
from macro import *
import dep
import reader

# convert our list to python list
def ll2pl(l):
    result = []
    for elem in l:
        result.append(elem)
    return result

def symbolArg(sym, what):
    if not sym.isSymbol():
        raise PyleArgTypeError(sym, what, 'symbol')
    return sym

def numberArg(val, what):
    if not val.isNumber():
        raise PyleArgTypeError(val, what, 'number')
    return val.numValue()

def functionArg(fun, what):
    if not fun.isFunction():
        raise PyleArgTypeError(fun, what, 'function')
    return fun

def stringArg(s, what):
    if s.isString():
        return s.strValue()
    else:
        return str(s)

def errorArg(err, what):
    if not err.isError():
        raise PyleArgTypeError(err, what, 'error')
    return err

def Pyle_apply(arglist):
    """@desc args: FUNCTION ARGS; result: list
    Apply FUNCTION to ARGS and return the result.
    """
    func = dep.evalFun(arglist.car())
    values = arglist.cdr().car()
    return func.call(values, values.length())

def Pyle_boundp(arglist):
    """@desc args: SYMBOL; result: nil/t
    Return t if SYMBOL is bound to a value, nil otherwise.
    """
    sym = symbolArg(arglist.car(), "boundp: ")
    if sym.boundp():
        return T
    else:
        return Nil

def Pyle_car(arglist):
    """@desc args: PAIR; result: car of pair
    Return the contents of the address part of the PAIR register.
    """
    return arglist.car().car()

def Pyle_catch(arglist):
    """@desc args: TAG BODYFORMS...; result: tag value; special: yes
    Eval BODYFORMS as implicit progn. If a throw occurs to the TAG,
    return the value that is thrown.
    """
    tag, bodyforms = arglist.cxr()
    try:
        result = dep.evalProgn(bodyforms)
    except PyleThrow as throw:
        if throw.tag() is dep.Eval(tag):
            return throw.value()
        else:
            raise throw
    return result

def Pyle_cdr(arglist):
    """@desc args: PAIR; result: cdr of pair
    Return the contents of the decrement part of the PAIR register.
    """
    return arglist.car().cdr()

def Pyle_cond(arglist):
    """@desc args: CLAUSE1...; special: yes; result: result value
    (cond (test-form value-forms...) ...)
    For each clause, evaluate test-form, and for the first that is
    true, return the last value of evaluating value-forms.
    """
    for clause in arglist:
        testform, valueforms = clause.cxr()
        if dep.Eval(testform):
            return dep.evalProgn(valueforms)
    return Nil

def Pyle_cons(arglist):
    """@desc args: CAR CDR; result: cons cell
    Return a cons cell constructed from the CAR and CDR args.
    """
    car, cdr = arglist
    return cons(car, cdr)

def Pyle_defmacro(arglist):
    """@desc args: SYMBOL ARGLIST [DOCSTRING] BODYFORMS...; special: yes; result: macro symbol
    Define SYMBOL as a macro.
    """
    symbol, arglist = arglist.cxr()
    args, body = arglist.cxr()
    fun = Macro(symbolArg(symbol, "defmacro: macro name"), args, body)
    symbol.setFunction(fun)
    return symbol

def Pyle_describe(arglist):
    """@desc args: OBJ; result: description list
    Return a list of OBJ's attributes.
    """
    return p2l(arglist.car().describe())

def Pyle_debug(arglist):
    """@desc args: FLAG...; return: flags
    Set or get the active debug flags.
    A single argument of nil resets all flags.
    """
    if arglist and arglist.cdr() is Nil and arglist.car() is Nil:
        dbg.reset()
    else:
        for flag in arglist:
            dbg.addFlag(str(flag))
    lc = ListCollector()
    for flag in dbg.getFlags():
        lc.add(intern(flag))
    return lc.list()

def Pyle_div(arglist):
    """@desc name: /; args: NUMBER1 ...; result: ratio
    Divide the first argument by the following ones.
    If there is only one argument, return its inverse.
    """
    arg1, arglist = arglist.cxr()
    start = numberArg(arg1, "/: first")
    if arglist is Nil:                    # no more arguments?
        return Number(1 / start)
    for arg in arglist:
        start /= numberArg(arg, "/: ")
    return Number(start)

def Pyle_doc(arglist):
    """@desc args: FUNCTION-OR-SYMBOL [RETURN]; result: docstring
    Print or return the docstring of a function (or a symbol's function).
    """
    funarg = arglist.car()
    do_return = arglist.cdr().car()
    if funarg.isFunction():
        func = funarg
    elif funarg.isSymbol():
        func = funarg.getFunction()
        if not func:
            raise PyleNoFunctionError(funarg)
    else:
        raise PyleArgTypeError(funarg, "doc:", "function or symbol")
    if do_return:
        return String(func.docstring())
    else:
        print(func.docstring())
        return Nil

def Pyle_eloop(arglist):
    """@desc args: BODYFORMS...; result: (does not return); special: yes
    This is an endless loop that can be exited only via throw or an error.
    It is intended as a building block for other types of loops using
    catch and throw.
    """
    while True:
        dep.evalProgn(arglist)
    return None

def Pyle_error(arglist):
    """@desc args: FORMAT ...; result: (does not return)
    Raise an error, with message formatted from FORMAT and the other arguments.
    """
    format, arglist = arglist.cxr()
    args = ll2pl(arglist)
    raise PyleUserError(stringArg(format, "error: "), *args)

def Pyle_errset(arglist):
    """@desc args: EXPR [PRINT-ERROR]; result: (value) or nil; special: yes
    Return the value of EXPR as a singleton list; on error return nil.
    In the latter case, the error object is in *last-error*, and, if
    optional PRINT-ERROR is non-nil or omitted, it is printed as well.
    """
    expr = arglist.car()
    print_error = (not arglist.cdr()) or arglist.cdr().car()
    try:
        value = dep.Eval(expr)
        return cons(value, Nil)
    except PyleError as e:
        if print_error:
            print(str(e))
        return Nil

def Pyle_eq(arglist):
    """@desc args: OBJ1 OBJ2; result: t/nil
    Return t if the arguments are the same object; nil otherwise.
    Two numbers or two strings of the same value are also considered eq.
    """
    obj1, obj2 = arglist
    return p2l(obj1.eq(obj2))

def Pyle_eval(arglist):
    """@desc args: EXPR; result: value
    Evaluate EXPR and return the result.
    """
    return dep.Eval(arglist.car())

def Pyle_format(arglist):
    """@desc args: PORT FORMAT-STRING ARG...; result: formatted string or nil
    Format a FORMAT-STRING with ARGs and return or print the result.
    The formatting is done via Python's {} formatting specifications, but
    only with positional arguments.
    If PORT is nil, return the resulting string; if it is t, print it;
    otherwise, do neither.
    """
    port, arglist = arglist.cxr()
    formatstring, arglist = arglist.cxr()
    formatted = str(formatstring).format(*ll2pl(arglist))
    if port is T:
        print(formatted, end='')
    elif port is Nil:
        return String(formatted)
    return Nil

def Pyle_function(arglist):
    """@desc args: FUNCTION-OR-SYMBOL; special: yes; result: function
    Return the function value of a symbol, or the argument if it is a function.
    """
    arg = arglist.car()
    if arg.isFunction():
        return arg
    elif arg.isSymbol():
        function = arg.getFunction()
        if function:
            return function
    else:
        raise PyleNoFunctionError(arg)

def Pyle_gensym(arglist):
    """@desc args: [prefix]; result: symbol
    Create and return a new, uninterned symbol.
    """
    prefix = str(arglist.car() or "G#")
    return Symbol.gensym(prefix)

def Pyle_intern(arglist):
    """@desc args: NAME; result: symbol
    Intern NAME as a symbol and return the symbol.
    """
    name = arglist.car()
    if name.isSymbol():
        return name
    return intern(str(name))

def Pyle_lambda(arglist):
    """@desc args: ARGS BODYFORMS...; special: yes; result: anon function
    Construct and return an anonymous lambda function.
    """
    params, body = arglist.cxr()
    return Form("*lambda*", params, body)

def Pyle_lessthan(arglist):
    """@desc name: <; args: NUM1 NUM2; result: nil/t
    Return t if NUM1 is less than NUM2, nil otherwise.
    """
    n1 = arglist.car().numValue()
    n2 = arglist.cdr().car().numValue()
    return p2l(n1 < n2)

def Pyle_let(arglist):
    """@desc args: BINDINGS BODYFORMS...; special: yes; result: result
    (let ((symbol1 [value1]) ...) bodyform ...)
    Evaluate BODY with local BINDINGS, return value of last BODY expressions.
    BINDINGS are of the form SYMBOL or (SYMBOL) or (SYMBOL VALUE), where the
    first two bind SYMBOL to nil. All VALUEs are evaluated before any variable
    bindings are done.
    """
    savedEnv = enterEnvironment()
    try:
        bindings, bodyforms = arglist.cxr()
        # dbg.p(dbg.let, "bindings:", bindings)
        # dbg.p(dbg.let, "body:", bodyforms)
        params = ListCollector()
        args = ListCollector()
        for binding in bindings:
            if binding.isSymbol():
                params.add(binding)
                args.add(Nil)
            else:
                var = binding.car()         # need not be symbol!
                value = binding.cdr().car() # Nil if value is unspecified
                params.add(var)
                args.add(dep.Eval(value))
        if bindings:
            bind_params(params.list(), args.list())
        return dep.evalProgn(bodyforms)
    finally:
        backtoEnvironment(savedEnv)

def Pyle_letrec(arglist):
    """@desc name: let*; args: BINDINGS BODYFORMS...; special: yes; result: result
    (let* ((symbol1 value1) ...) bodyform ...)
    Evaluate BODY with local BINDINGS, return value of last BODY expressions.
    BINDINGS are of the form SYMBOL or (SYMBOL) or (SYMBOL VALUE), where the
    first two bind SYMBOL to nil. VALUE is evaluated with bindings of earlier
    variables in the same let* already in place.
    """
    savedEnv = enterEnvironment()
    try:
        bindings, bodyforms = arglist.cxr()
        # dbg.p(dbg.let, "bindings:", bindings)
        # dbg.p(dbg.let, "body:", bodyforms)
        params = ListCollector() # need them in proper order
        args = ListCollector()
        for binding in bindings:
            if binding.isSymbol():
                params.add(binding)
                args.add(Nil)
            else:
                params.add(binding.car())
                args.add(binding.cdr().car())
        if bindings:
            bind_params(params.list(), args.list(), needEval=True)
        return dep.evalProgn(bodyforms)
    finally:
        backtoEnvironment(savedEnv)

def Pyle_list(arglist):
    """@desc args: ARGS...; result: args as a list
    Return the arguments as a list.
    """
    return arglist

def Pyle_listp(arglist):
    """@desc args: ARG; result: nil/t
    Return t if ARG is a list (nil or pair), nil otherwise.
    """
    return p2l(arglist.car().isList())

def Pyle_load(arglist):
    """@desc args: FNAME [MISSING-OK]; result: nil/t
    Load a file of code.
    """
    # import pdb; pdb.set_trace()
    fname, arglist = arglist.cxr()
    missing_ok = arglist.car()
    dep.load(fname.strValue(), missing_ok)
    return T

def Pyle_macroexpand(arglist):
    """@desc args: EXPR; result: expanded form
    Expand all macro calls in EXPR and return the expanded form.
    """
    return macroexpandForm(arglist.car())

def Pyle_macroexpand1(arglist):
    """@desc args: EXPR; result: expanded form
    Expand one layer of macro calls in EXPR and return the expanded form.
    """
    exp, hasepanded = expandFormRecurse(arglist.car())
    return exp

def Pyle_minus(arglist):
    """@desc name: -; args: NUMBER1 ...; result: subtraction
    Subtract the number arguments from the first.
    If there is only one argument, return its negation.
    """
    arg1, arglist = arglist.cxr()
    start = numberArg(arg1, "-: first")
    if arglist is Nil:                    # no more args?
        return Number(0 - start)
    for arg in arglist:
        start -= numberArg(arg, "-: ")
    return Number(start)

def Pyle_mult(arglist):
    """@desc name: *; args: NUMBER1...; result: product
    Multiply the number arguments.
    """
    start = 1
    for arg in arglist:
        start *= numberArg(arg, "*: ")
    return Number(start)

def Pyle_null(arglist):
    """@desc args: ARG; result: nil/t
    Return t if the result is nil, nil otherwise.
    """
    return p2l(not arglist.car())

def Pyle_plist(arglist):
    """@desc args: SYMBOL; result: property list
    Return the property list of SYMBOL.
    """
    props = symbolArg(arglist.car(), "plist: ").props()
    lc = ListCollector()
    for key, value in props.items():
        lc.add(cons(key, value))
    return lc.list()

def Pyle_plus(arglist):
    """@desc name: +; args: NUMBER1...; result: sum
    Add up the number arguments.
    """
    start = 0
    for arg in arglist:
        start += numberArg(arg, "+: ")
    return Number(start)

def Pyle_princ(arglist):
    """@desc args: ARG; result: string
    Print the argument to stdout without quoting; return the resulting string.
    """
    ob = arglist.car()
    if ob.isString():
        argstr = ob.strValue()
    else:
        argstr = str(ob)
        ob = String(argstr)
    print(argstr, end='')
    return ob

def Pyle_putprop(arglist):
    """@desc args: SYMBOL VALUE PROP; result: value
    Set SYMBOL's property PROPERTY to VALUE and return VALUE.
    """
    sym, arglist = arglist.cxr()
    value, arglist = arglist.cxr()
    prop = arglist.car()
    symbolArg(sym, "get: ").putprop(prop, value)
    return value

def qq_recurse(form):
    if not form.isPair():
        return form
    head, tail = form.cxr()
    if head is UnquoteSymbol:
        if tail.length() == 1:
            return dep.Eval(tail.car())
        else:
            raise PyleArgCountError(head, tail.length(), 1)
    tailResult = qq_recurse(tail)
    if not head.isPair():
        return cons(head, tailResult)
    headhead = head.car()
    if headhead is UnquoteSplicingSymbol:
        if head.length() == 2:
            unqSArg = dep.Eval(head.cdr().car())
            if not unqSArg.isProper():
                raise PyleArgTypeError(unqSArg, UnquoteSplicingSymbol, "list")
            # append rest of list nconc-style
            if unqSArg is Nil:
                return tailResult
            lastpair = unqSArg
            while True:
                nextlast = lastpair.cdr()
                if nextlast is Nil:
                    break
                lastpair = nextlast
            lastpair.rplacd(tailResult)
            return unqSArg
        else:
            raise PyleArgCountError(headhead, head.length() - 1, 1)
    else:
        return cons(qq_recurse(head), tailResult)

def Pyle_quasiquote(arglist):
    """@desc args: EXPR; result: expanded form; special: yes
    In EXPR, replace unquoted items them with their values as appropriate.
    Return the resulting form. Unquoted items are those preceded by an
    "unquote" sign (",") or an "unquote-splicing" (",@"). In the latter case,
    if the value is a list, splice it into the surrounding list.
    """
    return qq_recurse(arglist.car())

def Pyle_quote(arglist):
    """@desc special: yes; args: ARG; result: arg
    Return the argument, unevaluated.
    """
    return arglist.car()

def Pyle_read(arglist):
    """@desc args: [STRING]; result: object
    Read an s-expression from the specified string (or stdin) and return it.
    """
    s = arglist.car()
    if s:
        port = reader.StringPort(stringArg(s, "read: "))
        fname = "*string*"
    else:
        port = sys.stdin
        fname = "*stdin*"
    rdr = reader.Reader(port, fname, None)
    obj = rdr.read()
    rdr.close()
    return obj

def Pyle_rplaca(arglist):
    """@desc args: PAIR NEW-CAR; result: pair
    Replace the car of PAIR with NEW-CAR and return PAIR.
    """
    pair, newcar = arglist
    pair.rplaca(newcar)
    return pair

def Pyle_rplacd(arglist):
    """@desc args: PAIR NEW-CDR; result: pair
    Replace the cdr of PAIR with NEW-CDR and return PAIR.
    """
    pair, newcdr = arglist
    pair.rplacd(newcdr)
    return pair

def Pyle_set_function_name(arglist):
    """@desc name: set-function-name; args: FUNCTION NAME; result: name
    Set the name of FUNCTION to NAME and return NAME.
    """
    func, name = arglist
    functionArg(func, "set-function-name: function").setName(name)
    return name

def Pyle_set(arglist):
    """@desc args: SYMBOL VALUE; result: value
    Set SYMBOL to VALUE and return VALUE.
    """
    symbol, value = arglist
    envset(symbolArg(symbol, "set: variable"), value)
    return value

def Pyle_string(arglist):
    """@desc args: OBJECT ...; result: the result string
    Convert OBJECT into a string and return the result.
    In case of multiple arguments, concatenate all strings.
    """
    if arglist.cdr() is Nil:
        el = arglist.car()
        if el.isString():
            return el
        return String(str(el))
    return String("".join(map(str, arglist)))

def Pyle_string_length(arglist):
    """@desc name: string-length; args: STRING; result: length
    Return the number of characters in STRING.
    If the argument is not a string, it will be converted to one.
    """
    ob = arglist.car()
    if ob.isString():
        value = ob.strValue()
    else:
        value = str(ob)
    return Number(len(value))
        

def Pyle_stringp(arglist):
    """@desc args: OBJ; result: nil/t
    Return t if OBJ is a string, nil otherwise.
    """
    if arglist.car().isString():
        return T
    return Nil

def Pyle_string_match(arglist):
    """@desc name: string-match; args: REGEXP STRING; return: match list
    Match the REGEXP against STRING and return the list of matches.
    The first match is the match of the whole REGEXP, the other ones
    matches of the groups. If there was no match, return nil.
    """
    regexp, string = arglist
    match = re.search(str(regexp), str(string))
    if not match:
        return Nil
    lc = ListCollector()
    lc.add(String(match.group(0)))
    for group in match.groups():
        if group is None:
            lc.add(Nil)
        else:
            lc.add(String(group))
    return lc.list()

def Pyle_string_subst(arglist):
    """@desc name: string-subst; args: REGEXP REPL STRING [COUNT]; result: new string
    Replace matches of REGEXP in STRING by REPL and return the result.
    """
    regexp, arglist = arglist.cxr()
    repl, arglist = arglist.cxr()
    stringarg, arglist = arglist.cxr()
    countarg, arglist = arglist.cxr()
    if countarg is Nil:
        count = 0
    else:
        count = numberArg(countarg, "string-subst: count")
    string = str(stringarg)
    newstring = re.sub(str(regexp), str(repl), str(string), count)
    if newstring == string:
        return stringarg
    else:
        return String(newstring)

def Pyle_symbols(arglist):
    """@desc result: list of symbols
    Return a list of all symbols.
    """
    lc = ListCollector()
    for name in sorted(symbolTable):
        lc.add(symbolTable[name])
    return lc.list()

def Pyle_throw(arglist):
    """@desc args: tag resultform; result: (does not return)
    Throw TAG with the value of RESULTFORM into the innermost catch for TAG.
    """
    tag, value = arglist
    raise PyleThrow(tag, value)

def Pyle_type_of(arglist):
    """@desc name: type-of; args: OBJ; result: type symbol
    Return the type of OBJ as a symbol.
    """
    return intern(arglist.car().typename())

def Pyle_unquote(arglist):
    """@desc args: EXPR; special: yes
    Used to "unquote" (and evaluate) expressions in a quasiquote context.
    Throw an error when used outside of a quasiquote.
    """
    raise PyleUnquoteError(UnquoteSymbol)

def Pyle_unquote_splicing(arglist):
    """@desc name: unquote-splicing; args: EXPR; special: yes
    Used to "unquote" (and evaluate) expressions in a quasiquote context.
    Throw an error when used outside of a quasiquote.
    """
    raise PyleUnquoteError(UnquoteSplicingSymbol)

def Pyle_unwind_protect(arglist):
    """@desc name: unwind-protect; args: BODYFORM CLEANUP-FORM ...; return: value; special: yes
    Eval BODYFORM, and even in case of an error or throw, eval
    CLEANUPFORMS. If BODYFORM completes normally, return its value
    after executing the CLEANUPFORMS.
    """
    bodyform, cleanupforms = arglist.cxr()
    try:
        result = dep.Eval(bodyform)
    except Exception as e:
        dep.evalProgn(cleanupforms)
        raise e
    dep.evalProgn(cleanupforms)
    return result


def make_builtin(func):
    doclines = func.__doc__.splitlines()
    attrs = {
        "special": False,
        "args": "",
        "name": re.sub("Pyle_", "", func.__name__),
        "result": "",
    }
    # print("doclines[0] =", doclines[0])
    if doclines[0].startswith("@desc "):
        attr_line = re.sub("@desc ", "", doclines.pop(0))
        for attr in attr_line.split(";"):
            # print("attr is", attr)
            match = re.match(r"\s*([^:\s]+)\s*:\s*(\S.*)", attr)
            if match:
                key = match.group(1)
                value = match.group(2)
                attrs[key] = value
                # print("attrs[{0}] = {1}".format(key, value))
    # print(attrs)
    minargs = 0
    maxargs = 0
    args = attrs["args"].split()
    for arg in args:
        # print("param is", arg)
        if re.match(r"^\[\S+\]$", arg):
            # print("dingel")
            maxargs += 1
        elif re.search(r"\.\.\.$", arg):
            maxargs = None      # should be last arg, otherwise boom
        else:
            minargs += 1
            maxargs += 1
    builtin_name = attrs["name"]
    # replace keywords line with function synopsis
    isspecial = text2bool(attrs['special'])
    if attrs["result"]:
        result = " => " + attrs["result"]
    else:
        result = ""
    doclines.insert(0, "builtin {t} ({n} {a}){r}".\
                    format(n=builtin_name, a=" ".join(args), r=result,
                           t='special form' if isspecial else 'function'))
    for i in range(len(doclines)):
        doclines[i] = re.sub("^    ", "", doclines[i])
    if doclines[-1] == "":
        doclines = doclines[0:-1]
    func.__doc__ = "\n".join(doclines)
    builtin_func = Builtin(builtin_name, func, minargs, maxargs, isspecial)
    sym = intern(builtin_name)
    sym.setFunction(builtin_func)
    # dbg.p(dbg.bi, "builtin", sym, minargs, maxargs, sym.getFunction())

# must copy this to avoid "RuntimeError: dictionary changed size
# during iteration" :-(
things = copy.copy(globals())
for name in things:
    if name.startswith("Pyle_"):
        make_builtin(things[name])
