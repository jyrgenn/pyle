Things to do in Pyle
====================

*: todo; +: done; -: rejected; @: in progress

 * setf

----------
old items:

 + (symbol--function 'car) crash. This had two reasons: the
   parameters for `get' were in wrong order, and there was actually
   an attribute names None in sys:function's plist -- it was defined
   before for the symbol `id' for the object id, so the value of the
   Python variable IdPropSymbol was indeed None. This circular
   dependency didn't seem easy to come by, so I removed the id
   property of symbols.
 + dolist
 + dotimes
 - have tables, after all, so the testing doesn't have quadratic
   effort; not done, but replaced the existence checks by properties
   on the symbols, which do go through a map, no, table, no, dict;
   point is, you can se symbol properties as a map substitute
 + eliminate the nargs parameter from builtins
 + destructuring bind like lingo
 + read from string (and a read builtin at all)
 + unify parameter binding for functions and let
 + finally check if let isn't secretly (and wrongly) a let*; alas,
   it was, but that is fixed and now we have a proper let *and*
   let*..
 + PyleSyntaxError: pass fname, too
