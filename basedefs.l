;;; base macros and functions for pyle

(defmacro defun (symbol args . bodyforms)
  "Define a function named SYMBOL with ARGS list and BODYFORMS."
  (let ((funcsym (gensym)))
    `(let ((,funcsym (lambda ,args ,@bodyforms)))
       (putprop ',symbol ,funcsym 'sys:function)
       (set-function-name ,funcsym ',symbol))))

;; (defun car (l)
;;   (let (((first) l))
;;     first))

;; (defun cdr (l)
;;   (let (((first . rest) l))
;;     rest))
  
(defmacro setq (var value)
  `(set ',var ,value))

(defmacro funcall (func . args)
  `(apply ,func (list ,@args)))

(defmacro if (condition if-clause . else-clauses)
  "If CONDITION yields true, eval IF-CLAUSE and return its value.
Otherwise, eval ELSE-CLAUSES and return the result of the last."
  `(cond (,condition ,if-clause)
         (t ,@else-clauses)))

(defmacro when (condition . if-clauses)
  "If CONDITION yields true, eval IF-CLAUSES and return the result of the last."
  `(cond (,condition ,@if-clauses)
         (t nil)))

(defmacro unless (condition . else-clauses)
  "If CONDITION yields nil, eval ELSE-CLAUSES and return the value of the last."
  `(cond (,condition nil)
         (t ,@else-clauses)))

(defmacro 1- (n)
  "Return a number one less than the argument N."
  `(- ,n 1))

(defmacro 1+ (n)
  "Return a number one bigger than the argument N."
  `(+ ,n 1))

(defmacro progn bodyforms
  "Eval BODYFORMS and return the value of the last."
  `(let ()
     ,@bodyforms))

(defmacro prog1 (form1 . forms)
  "Eval body forms and return the value of the first."
  `(let ((value ,form1))
     ,@forms
     value))

(defmacro not (arg)
  "Return t if ARG is false, nil otherwise."
  `(null ,arg))

(defmacro and2 (arg1 arg2)
  `(when arg1
     (when arg2
       arg2)))

(defmacro while (cond . bodyforms)
  `(catch '*break-while*
     (eloop
      (unless ,cond
        (throw '*break-while* nil))
      ,@bodyforms)))

(defmacro break ()
  "Break out of a while loop."
  '(throw '*break-while* nil))

(defmacro push (item variable)
  "Prepend ITEM to the list in VARIABLE and store the result in VARIABLE.
Return the new value of VARIABLE."
  `(setq ,variable (cons ,item ,variable)))

(defmacro pop (variable)
  "Return the car of the value of VARIABLE;
store the cdr of the value into VARIABLE."
  (let ((valsym (gensym "value"))
        (tailsym (gensym "tail")))
    `(let ((,valsym (car ,variable))
           (,tailsym (cdr ,variable)))
       (setq ,variable ,tailsym)
       ,valsym)))

(defmacro with-gensyms (syms . body)
  "Run the BODY with the symbols in SYMS (a list) bound to gensyms.
This is meant to simplify macro definitions that would otherwise
use a
  (let ((param1 (gensym))
        (param2 (gensym)))
        ... )
    ,@body)
symbol definition chain explicitly."
  (let (decls)
    (while syms
      (let ((sym (pop syms)))
        (push (list sym '(gensym 'sym)) decls)))
    `(let ,decls
       ,@body)))

(defmacro and args
  "Evaluate ARGS until one is nil; return the last evaluated value."
  (with-gensyms (lsym elsym rsym)
    `(let ((,lsym ',args)
           (,rsym t))
       (while ,lsym
         (let ((,elsym (eval (pop ,lsym))))
           (setq ,rsym ,elsym)
           (unless ,elsym
             (break))))
       ,rsym)))

(defmacro or args
  "Evaluate ARGS until one is true; return the last evaluated value."
  (with-gensyms (lsym elsym rsym)
    `(let ((,lsym ',args)
           ,rsym)
       (while ,lsym
         (let ((,elsym (eval (pop ,lsym))))
           (when ,elsym
             (setq ,rsym ,elsym)
             (break))))
       ,rsym)))

(defmacro symbol-name (sym)
  `(string ,sym))

(defmacro dolist ((var listform resultform) . bodyforms)
  "Iterate over LISTFORM, binding symbol VAR to each element in turn.
Return the value of RESULTFORM (if specified) or nil."
  (with-gensyms (list)
    `(let ((,list ,listform))
       (while ,list
         (let ((,var (pop ,list)))
           ,@bodyforms))
       ,resultform)))

(defmacro dotimes ((var countform resultform) . bodyforms)
  "dotimes evaluates COUNTFORM, which should produce an integer. If
COUNTFORM is zero or negative, the body is not executed. dotimes then
executes the body once for each integer from 0 up to but not including
the value of COUNTFORM, in the order in which the statements occur,
with VAR bound to each integer. Then RESULTFORM is evaluated. At the
time RESULTFORM is processed, VAR is bound to the number of times the
body was executed. [CLHS]"
  (with-gensyms (end)
    `(let ((,var 0)
           (,end ,countform))
       (while (< ,var ,end)
         ,@bodyforms
         (incf ,var))
       ,resultform)))

(defmacro dotimes1 ((var countform resultform) . bodyforms)
  "dotimes evaluates COUNTFORM, which should produce an integer. If
COUNTFORM is less than or equal to 1, the body is not executed.
dotimes then executes the body once for each integer from 1 up to
including the value of COUNTFORM, in the order in which the statements
occur, with VAR bound to each integer. Then RESULTFORM is evaluated.
At the time RESULTFORM is processed, var is bound to the number of
times the body was executed."
  (with-gensyms (end)
    `(let ((,var 1)
           (,end ,countform))
       (while (<= ,var ,end)
         ,@bodyforms
         (incf ,var))
       ,resultform)))

(defmacro for ((var from to step test) . bodyforms)
  "The for loop uses VAR as the counter variable, starting with FROM,
adding STEP to VAR after each run, ending when (TEST VAR TO) no longer
is true. The default STEP is 1; the default TEST is #'<. The arguments
FROM, TO, STEP, and TEST are evaluated just once."
  (with-gensyms (stepval testfunc end)
    `(let ((,stepval (or (eval ,step) 1))
           (,testfunc (or (eval ,test) #'<))
           (,var (eval ,from))
	   (,end (eval ,to)))
       (while (funcall ,testfunc ,var ,end)
         ,@bodyforms
         (incf ,var ,stepval)))))

(defmacro symbolp (ob)
  `(eq (type-of ,ob) 'symbol))

(defmacro consp (ob)
  `(eq (type-of ,ob) 'pair))

(defmacro rplaca-ret-value (pair new-car)
  `(car (rplaca ,pair ,new-car)))

(defmacro rplacd-ret-value (pair new-cdr)
  `(cdr (rplacd ,pair ,new-cdr)))

;;; the setf stuff... will I get it to work?

(defmacro incf (place . rest)
  "Increment PLACE by optional DELTA (or 1) and return the new value."
  (let ((delta (or (car rest) 1)))
    `(setf ,place (+ ,place ,delta))))

(defmacro decf (place . rest)
  "Decrement place by optional DELTA (or 1) and return the new value."
  (let ((delta (car rest)))
    (if delta
        `(setf ,place (- ,place ,delta))
      `(setf ,place (1- ,place)))))

(setq *setf-update-alist* '())

(defmacro update-setf-table (access update)
  `(setq *setf-update-alist* (cons (cons ,access ,update) *setf-update-alist*)))

(defmacro defsetf (access-fn update-fn-or-lambda-list . forms)
  "Define an update-function for an access-function, to be used by setf.
Short form:
    (defsetf access-fn update-fn)
where both ACCESS-FN and UPDATE-FN are function symbols.

Long form:
    (defsetf access-fn (lambda-list) forms ...)
defines an update function with the parameters specified in the
LAMBDA-LIST, and the forms executed for update in a body where
these parameters are bound accordingly.

In both cases the update function is called with the arguments of the access
function plus the value argument."
  `(progn
     ,(if (symbolp update-fn-or-lambda-list)
          (if forms
              (error "defsetf: two-symbol form called with forms argument(s)")
            `(update-setf-table ',access-fn ',update-fn-or-lambda-list))
        `(update-setf-table ',access-fn
                            (lambda ,update-fn-or-lambda-list ,@forms)))
      ',access-fn))

(defsetf car rplaca-ret-value)
(defsetf cdr rplacd-ret-value)
(defsetf caar (l value) (rplaca-ret-value (car l) value))
(defsetf cadr (l value) (rplaca-ret-value (cdr l) value))
(defsetf cdar (l value) (rplacd-ret-value (car l) value))
(defsetf cddr (l value) (rplacd-ret-value (cdr l) value))
(defsetf caaar (l value) (rplaca-ret-value (caar l) value))
(defsetf caadr (l value) (rplaca-ret-value (cadr l) value))
(defsetf cadar (l value) (rplaca-ret-value (cdar l) value))
(defsetf caddr (l value) (rplaca-ret-value (cddr l) value))
(defsetf cdaar (l value) (rplacd-ret-value (caar l) value))
(defsetf cdadr (l value) (rplacd-ret-value (cadr l) value))
(defsetf cddar (l value) (rplacd-ret-value (cdar l) value))
(defsetf cdddr (l value) (rplacd-ret-value (cddr l) value))
(defsetf caaaar (l value) (rplaca-ret-value (caaar l) value))
(defsetf caaadr (l value) (rplaca-ret-value (caadr l) value))
(defsetf caadar (l value) (rplaca-ret-value (cadar l) value))
(defsetf caaddr (l value) (rplaca-ret-value (caddr l) value))
(defsetf cadaar (l value) (rplaca-ret-value (cdaar l) value))
(defsetf cadadr (l value) (rplaca-ret-value (cdadr l) value))
(defsetf caddar (l value) (rplaca-ret-value (cddar l) value))
(defsetf cadddr (l value) (rplaca-ret-value (cdddr l) value))
(defsetf cdaaar (l value) (rplacd-ret-value (caaar l) value))
(defsetf cdaadr (l value) (rplacd-ret-value (caadr l) value))
(defsetf cdadar (l value) (rplacd-ret-value (cadar l) value))
(defsetf cdaddr (l value) (rplacd-ret-value (caddr l) value))
(defsetf cddaar (l value) (rplacd-ret-value (cdaar l) value))
(defsetf cddadr (l value) (rplacd-ret-value (cdadr l) value))
(defsetf cdddar (l value) (rplacd-ret-value (cddar l) value))
(defsetf cddddr (l value) (rplacd-ret-value (cdddr l) value))
(defsetf elt setelt)
(defsetf symbol-function fset)
(defsetf symbol-value set)
(defsetf get propput)
(defsetf nth (n l value) (rplaca-ret-value (nthcdr n l) value))

(defmacro setf (place value)
  "Set field at PLACE to VALUE."
  (cond ((symbolp place)
         `(setq ,place ,value))
        ((consp place)
         (let* ((access-fn (car place))
                (args (cdr place))
                (update-fn (cdr (assoc access-fn *setf-update-alist*))))
           (if update-fn
               `(,update-fn ,@args ,value)
             (error "no setf expansion found for place {}" place))))
        (t (error "no setf expansion for place {}" place))))

(defmacro nth (n l)
  `(car (nthcdr ,n ,l)))

(defmacro elt (l n)
  `(nth ,n ,l))

(defmacro setelt (l n val)
  `(rplaca-ret-value (nthcdr ,n ,l) ,val))

(defmacro symbol-value (sym)
  `(eval 'sym))

;;; functions

(defun fset (symbol function)
  (putprop symbol function 'sys:function))

;; some function aliases
(fset '= #'eq)
(fset 'error-attrs #'describe)

(defun propput (sym prop val)
  "Give SYMBOL a PROPERTY of VALUE and return VALUE.
A putprop by another name and another argument order,
for the benefit of setf."
  (putprop sym val prop))

(defun nthcdr (n l)
  (while (< 0 n)
    (pop l)
    (decf n))
  l)

(defmacro defparameter (sym . maybeval)
  (let (((val) maybeval))
    `(setq ,sym ,val)))

(defun terpri () (princ "\n"))

(defun error-message (err)
  "Return the error message from the error object ERR."
  (cdr (assoc 'message (error-attrs err))))

;; number and string comparisons; = and < are builtins

(defun <= (n1 n2)
  "Return t if number N1 is less or equal to number N2, nil otherwise."
  (or (< n1 n2) (= n1 n2)))

(defun > (n1 n2)
  "Return t if number N1 is greater than number N2, nil otherwise."
  (not (<= n1 n2)))

(defun >= (n1 n2)
  "Return t if number N1 is greater than number N2, nil otherwise."
  (not (< n1 n2)))

(defun /= (n1 n2)
  "Return t if number N1 is different from number N2, nil otherwise."
  (not (= n1 n2)))

(defun iota (count . rest)
  "Return a list of numbers of length COUNT.
The list starts with optional START (default 0), with each element being
bigger than the previous one by optional STEP (default 1)."
  (if (zerop count)
      ()
    (let ((lc (list-collector))
          (start (or (car rest) 0))
          (step (or (car (cdr rest)) 1)))
      (funcall lc start)
      (incf start step)
      (decf count)
      (while (> count 0)
        (funcall lc start)
        (incf start step)
        (decf count))
      (funcall lc))))

(defun reverse (l)
  "Reverse the list L and return it. The original L will be unchanged."
  (let ((result nil))
    (while l
      (push (car l) result)
      (setq l (cdr l)))
    result))

(defun nreverse (l)
  "Reverse the list L by modifying the cdrs of the pairs and return it."
  (let (result nextl)
    (while l
      (setq nextl (cdr l))
      (rplacd l result)
      (setq result l)
      (setq l nextl))
    result))

(defun member (item l)
  "Return the part of the list L that has ITEM as its first element. Or nil."
  (if (null l)
      nil
    (if (eq item (car l))
        l
      (member item (cdr l)))))

(defun caar (l) (car (car l)))
(defun cadr (l) (car (cdr l)))
(defun cdar (l) (cdr (car l)))
(defun cddr (l) (cdr (cdr l)))

(defun caaar (l) (car (car (car l))))
(defun caadr (l) (car (car (cdr l))))
(defun cadar (l) (car (cdr (car l))))
(defun caddr (l) (car (cdr (cdr l))))
(defun cdaar (l) (cdr (car (car l))))
(defun cdadr (l) (cdr (car (cdr l))))
(defun cddar (l) (cdr (cdr (car l))))
(defun cdddr (l) (cdr (cdr (cdr l))))

(defun caaaar (l) (car (car (car (car l)))))
(defun caaadr (l) (car (car (car (cdr l)))))
(defun caadar (l) (car (car (cdr (car l)))))
(defun caaddr (l) (car (car (cdr (cdr l)))))
(defun cadaar (l) (car (cdr (car (car l)))))
(defun cadadr (l) (car (cdr (car (cdr l)))))
(defun caddar (l) (car (cdr (cdr (car l)))))
(defun cadddr (l) (car (cdr (cdr (cdr l)))))
(defun cdaaar (l) (cdr (car (car (car l)))))
(defun cdaadr (l) (cdr (car (car (cdr l)))))
(defun cdadar (l) (cdr (car (cdr (car l)))))
(defun cdaddr (l) (cdr (car (cdr (cdr l)))))
(defun cddaar (l) (cdr (cdr (car (car l)))))
(defun cddadr (l) (cdr (cdr (car (cdr l)))))
(defun cdddar (l) (cdr (cdr (cdr (car l)))))
(defun cddddr (l) (cdr (cdr (cdr (cdr l)))))


(defun assoc (item alist)
  "Return the first pair of ALIST where the car is eq to ITEM."
  (let (result)
    (while alist
      (let ((first-pair (pop alist)))
        (when (eq (car first-pair) item)
          (setq result first-pair)
          (break))))
    result))

(defun functionp (ob)
  (let ((type (type-of ob)))
    (cond ((eq type 'function) t)
          ((eq type 'builtin) t)
          ((eq type 'form) t)
          (t nil))))

(defun make-list (n el)
  "Return a list of length N with elements EL (which may be a function)."
  (let ((lc (list-collector)))
    (if (functionp el)
        (while (< 0 n)
          (funcall lc (funcall el))
          (setq n (1- n)))
      (while (< 0 n)
        (funcall lc el)
        (setq n (1- n))))
    (funcall lc)))

(defun get (symbol property)
  (let ((pair (assoc property (plist symbol))))
    (if (null pair)
        nil
      (cdr pair))))

(defun map (f l)
  "Return a list of the results of applying function F to each element of L."
  (if (null l)
      nil
    (cons (funcall f (car l))
          (map f (cdr l)))))

(defun map2 (f l)
  (let ((lc (list-collector)))
    (while l
      (funcall lc (funcall f (car l)))
      (setq l (cdr l)))
    (funcall lc)))

(defun zerop (ob)
  "Return t if OB is zero, nil otherwise."
  (eq ob 0))

(defun list-length (l)
  "Return the length of list L."
  (let ((len 0))
    (while l
      (pop l)
      (incf len))
    len))

(defun length (ob)
  "Return the length of string or list OB."
  (cond ((stringp ob) (string-length ob))
        ((listp ob) (list-length ob))
        (t (error "cannot take length of object: {}" ob))))

(defun copy (ob)
  "Return a deep copy of object OB."
  (if (atom ob)
      ob
    (cons (copy (car ob))
          (copy (cdr ob)))))

(defun filter (predicate l)
  "Return a list of the elements of L for which PREDICATE is true."
  (let ((lc (list-collector)))
    (while l
      (let ((el (pop l)))
        (when (funcall predicate el)
          (funcall lc el))))        
    (funcall lc)))

(defun list-collector ()
  "Create a list collector closure and return it as a function.

The returned function takes an arbitrary number of arguments, which
are then added to the end of the list, and returns the list. The
normal use case would be to call it a number of times to add items to
the list, and then call it once without arguments to return the
resulting list. This is more efficient than using append repeatedly.

Example:
    (let ((lc (list-collector)))
      (funcall lc 'see)
      (funcall lc 0 'evil)
      (funcall lc 'hear \"no\" 'evil)
      (funcall lc))
=> (see no evil hear no evil)
"
  (let (the-list lastpair)
    (lambda new
      (while new
        (let ((newpair (cons (pop new) nil)))
          (if the-list
              (rplacd lastpair newpair)
            (setq the-list newpair))
          (setq lastpair newpair)))
      the-list)))

(defun print (arg)
  "Print ARG without quoting, preceded by a newline and followed by a blank."
  (princ (string "\n" arg " ")))

(defun join (joint elems)
  "Append all ELEMS (as strings) joined by JOINT to a string and return it."
  (if (null elems)
      ""
    (if (cdr elems)
        (let ((lc (list-collector)))
          (funcall lc (pop elems))
          (while elems
            (funcall lc joint)
            (funcall lc (string (pop elems))))
          (apply #'string (funcall lc)))
      (string (car elems)))))

(defun apropos (arg)
  "Print the known symbols matching ARG (a regexp)."
  (let ((syms (apropos-list arg)))
    (while syms
      (let* ((sym (pop syms))
             (is-fbound (fboundp sym))
             qualifiers)
        (when (> (length (plist sym)) (if is-fbound 2 1))
          (push "other attrs" qualifiers))
        (when (boundp sym)
          (push "variable" qualifiers))
        (when is-fbound
          (push "function" qualifiers))
        (format t "{:32}  {}\n" sym (join ", " qualifiers))))))

(defun apropos-list (arg)
  "Return a list of known symbols matching ARG (a regexp)."
  (filter (lambda (sym) (string-match arg sym)) (symbols)))

(defun symbol-function (sym . rest)
  "Return the function of symbol SYM.
If optional NOERROR is non-nil, return nil if no such function exists.
Otherwise, raise an error."
  (let ((func (get sym 'sys:function)))
    (if func
        func
      (unless (car rest)
        (error "symbol {} has no function value" sym)))))

(defun fboundp (sym)
  (symbol-function sym 'noerror))

