# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

# utility functions and things, but without knowing objects

import re

def text2bool(arg):
    """Return a boolean value that is appropriate for the argument.

    All of yes, no, on, off, true, false, 0, 1 are handled as
    expected. Also, the Python objects False, True, None, 0, 1 and
    the empty string are handled as appropriate. If it is none of
    these, throw a KeyError exception.
    """
    if arg in (True, False, None, 0, 1, ""):
        return not not arg
    arg = str(arg).lower()
    if re.match("(yes|on|true|1)$", arg):
        return True
    if re.match("(no|off|false|0)$", arg):
        return False
    raise KeyError("cannot match '{0}' to boolean values".format(arg))

# replace Capital letter in the middle of the string by their
# lowercase equivalent, preceded by a dash
def dashify(name):
    newname = []
    for c in name:
        if c.isupper() and newname:
            newname.append("-")
        newname.append(c.lower())
    return "".join(newname)

def makeString(length, init=" "):
    ilen = len(init)
    ii = 0
    result = []
    for i in range(length):
        result.append(init[ii])
        ii = (ii + 1) % ilen
    return "".join(result)
