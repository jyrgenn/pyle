;;; test input data

;; read0
a

;; read1
12

;; read2
"333333r"

;; read3
'(3 4 5)

;; read4
(let ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
  (let ((e b) (g e))
    (list e g)))
