# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import dbg
from utils import *
from exceptions import *
import dep
from object import *
from stdsyms import *

def expandList(list, level):           # return new list, haveExpanded
    # dbg.p(dbg.mac, "expandList:", list)
    haveExpanded = False
    lc = ListCollector()
    # cannot use simple iteration over the list, which would not
    # handle improper lists
    while list.isPair():
        elem, did = expandFormRecurse(list.car(), level)
        lc.add(elem)
        haveExpanded = haveExpanded or did
        list = list.cdr()
    if list:                    # last cdr of improper list
        elem, did = expandFormRecurse(list, level)
        lc.addEnd(elem)
        haveExpanded = haveExpanded or did
    return lc.list(), haveExpanded

def expandFormRecurse(form, level=""):    # return expanded form, haveExpanded
#    print(level, "expandFormRecurse:", form)
    # dbg.p(dbg.mac, "expandFormRecurse:", form)
    if form.isPair():
        head = form.car()
        if head.isSymbol():
            args = form.cdr()
            maybeMacro = head.getFunction()
            if maybeMacro and maybeMacro.isMacro():
                result = maybeMacro.expand(args), True
            else:
                expargs, did = expandList(args, level + "  ")
                result = cons(head, expargs), did
        else:
            result = expandList(form, level + "  ")
    else:
        result = form, False
#    print(level, "=>", result)
    return result

def macroexpandForm(form):
    needExpand = True
    while needExpand:     # I'd still rather have a real post-checked loop
        form, needExpand = expandFormRecurse(form)
    return form
