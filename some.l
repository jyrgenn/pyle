(defun 3x (n) (* n 3))

(defun map (f l)
  (cond ((null l) nil)
        (t (cons (apply f (list (car l)))
                 (map f (cdr l))))))

(defun zerop (ob)
  (eq ob 0))

(map #'3x '(3 4 5 6))

(defun fac (n)
  (cond ((zerop n) 1)
        (t (* n (fac (- n 1))))))

(funcall #'+ 1 2 3 4 5 6 7 8 9 10)
(funcall #'* 1 2 3 4 5 6 7 8 9 10)

(let ((a 'lexical))                     ; binding (1)
  (let ((f (lambda () a)))
    (let ((a 'dynamic))                 ; binding (2)
      (funcall f))))

(defun fib (n)
  (cond ((< n 2) n)
        (t (+ (fib (- n 1))
              (fib (- n 2))))))

(fib 1)

#'list-collector
13
(setq lc (list-collector))
lc
(funcall lc 'foo)
(funcall lc "new")
(funcall lc 120)
(funcall lc)

(let ((a 119) (b 22) (c 23))
  `(+ ,a ,b ,@(list b c) ,(* 2 77)))
;; error: b (22) is not a list
;; (let ((a 119) (b 22) (c 23))
;;   `(+ ,a ,b ,@b c ,(* 2 77)))

;; (load "macros")

(princ "if:\n")
(if (princ 2) (princ 3) (princ 4))
(princ "\nwhen:\n")
(when (princ 2) (princ 3))
(princ "\nunless:\n")
(unless (princ 2) (princ 3))

(princ "\nfoo\n")
(princ "\n-------------\n")

(princ "progn\n")
(princ (progn (princ (* 3 4))
              (princ (- 7 15))
              (princ (+ 9 18))
              (princ "\nhuhu\n")))
(princ (prog1 (princ (* 3 4))
              (princ (- 7 15))
              (princ (+ 9 18))
              (princ "\nhuhu\n")))

(princ "\n\n")
(princ (string 'see " " 123 " no evil" "\n"))

(princ (symbol-name 'symbol-name))
(princ "\n\n")

(print (catch 'exit
         (unwind-protect
             (progn (print "protected form")
                    (throw 'exit (+ 119 5))
                    (print "aww, we won't see this..."))
           (print "lala")
           (print 'lulu))))
(princ "\n")

(print (unwind-protect
           ;; this is supposed to throw an error, so we can see if the
           ;; cleanup-form is indeed evaluated
           (car)
         (print "cleanup-form\n\n")))

(defun fset (sym func)
  (putprop sym func 'function))

(fset 'fac (lambda (n)
             (if (zerop n)
                 1
               (* n (fac (1- n))))))

(let ((n 7))
  (princ (string "fac(" n ") = " (fac n) "\n")))

(putprop 'mapcar #'map 'sys:function)
(defun 3* (n) (* 3 n))

(princ (string (mapcar #'fac '(1 2 3 4 5 6 7 8 9 10)) "\n"))

(princ (string (get 'foodeldeedoodel ':sys:name) "\n"))
