# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import dep
from object import *

# syntactically significant symbols
FunctionSymbol = intern("function")
LambdaSymbol = intern("lambda")
QuasiquoteSymbol = intern("quasiquote")
QuoteSymbol = intern("quote")
UnquoteSplicingSymbol = intern("unquote-splicing")
UnquoteSymbol = intern("unquote")

EnableTracebackSymbol = intern("*enable-traceback*")
EnableTracebackSymbol.defvar(Nil)

def enableTraceback(on=True):
    EnableTracebackSymbol.defvar(p2l(on))

DefmacroSymbol = intern("defmacro")

LastErrorSymbol = intern("*last-error*")
LastErrorSymbol.defvar(Nil)

def makeError(e):
    envset(LastErrorSymbol, Error(e))
dep.makeError = makeError
    
