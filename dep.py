# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

"""To work around circular dependencies."""

Eval = None
evalProgn = None
evalArgs = None
repl = None
load = None
makeError = None
exit_on_error = None
interactive = None
