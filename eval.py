# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import re

import dbg
from exceptions import * 
from object import *
import builtin
import dep
from utils import *

def evalProgn(oblist):
    # dbg.p(dbg.eval, "evalprogn", oblist)
    result = Nil
    for ob in oblist:
        result = Eval(ob)
    return result

def evalFun(funob):
    # dbg.p(dbg.eval, "evalFun", funob)
    if funob.isSymbol():
        func = funob.getFunction()
        if func:
            return func
        raise PyleNoFunctionError(funob)
    elif funob.isFunction():
        return funob
    else:
        raise PyleNoFunctionError(funob)

def evalArgs(oblist):
    # dbg.p(dbg.eval, "evalargs", oblist)
    length = 0
    lc = ListCollector()
    for ob in oblist:
        length += 1
        lc.add(Eval(ob))
    # dbg.p(dbg.eval, "evalArgs returns", lc.list(), length)
    return lc.list(), length

level = 0
envs = []

def Eval(ob):
    global level
    global envs
    oldlevel = level
    level += 1
    envs.append(CurrentEnv)
    try:
        # dbg.p(dbg.eval, "{l}Eval {o} in {e}".format(o=ob, e=CurrentEnv,
        #                                   l=makeString(2 * level)))
        if ob.isSymbol():
            result = ob.value()
        elif ob.isPair():
            funob, arglist = ob.cxr()
            function = evalFun(funob)
            if function.isSpecial():
                length = arglist.length()
            else:
                arglist, length = evalArgs(arglist)
            result = function.call(arglist, length)
        else:
            result = ob
        # dbg.p(dbg.eval, "{l}<= {r}".format(r=result, l=makeString(2 * level)))
        return result
    except PyleThrow as throw:
        raise throw
    except Exception as e:
        if dep.interactive or dep.evalstack:
            print("#{l}: {e}\n    {ob}".format(e=envs[-1], l=level, ob=ob))
        raise e
    finally:
        envs.pop()
        level = oldlevel
    

dep.evalArgs = evalArgs
dep.evalProgn = evalProgn
dep.evalFun = evalFun
dep.Eval = Eval
