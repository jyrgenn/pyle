;; This file is part of the Python Lambda Experiment (pyle),
;; Copyright (C) 2017 Juergen Nickelsen <ni@w21.org>.
;; See LICENSE for the conditions under which this code is licensed to you.

(format t "loading the tests\n")

(setq testcount 0)
(setq fails ())
(setq dup-test-names ())

(defmacro test (name expr expect)
  "Have a test with NAME, eval EXPR, which should be the same as EXPECT."
  `(let ((value (string (eval ',expr)))
         (exp (string ',expect))
         (thename (string ',name)))
     (setq testcount (1+ testcount))
     (format t "{}: {:20} => {}\n" testcount thename value)
     ;; (when (member thename test-names)
     ;;     (format t "\nWARN test {} already seen\n" thename))
     ;; (push thename test-names)
     (let ((namesym (intern thename)))
       (when (get namesym 'has-test)
         (format t "\nWARN test {} already seen\n" thename)
         (push thename dup-test-names))
       (putprop namesym t 'has-test))
     (unless (eq value exp)
       (push thename fails)
       (format t "\nFAIL {}:\n" thename)
       (format t "   expect: {}\n" exp)
       (format t "     have: {}\n" value))))

;; different symbols
(test eq0 (eq 'foo 'bar) nil)
;; same symbol
(test eq1 (eq 'foo 'foo) t)
;; same symbol bound
(test eq2 (let ((har 'harhar))
            (eq har har))
      t)
;; same symbol bound differently
(test eq3 (let ((har 'harhar)
                (bar 'harhar))
            (eq har bar))
      t)
;; symbol and a string
(test eq4 (let ((a 'the-symbol))
            (let ((b (string a)))
              (eq a b)))
      nil)

;; same string object
(test eq5 (let ((a "the string"))
            (let ((b a))
              (eq a b)))
      t)
;; same string, two objects
(test eq6 (let ((a "the-string"))
            (let ((b (string 'the-string)))
              (eq a b)))
      t)
;; same number object
(test eq7 (let ((n 120))
            (let ((m n))
              (eq n m)))
      t)
;; different number objects
(test eq8 (let ((n 120)
                (m (* 3 40)))
            (eq n m))
      t)
;; different numbers
(test eq9 (let ((n 120)
                (m (* 3 41)))
            (eq n m))
      nil)

(test apply (apply #'+ (list 3 4 5)) 12)

(test boundp (let ((this-is 'notyet))
               (list (boundp 'this-is) (boundp 'notyet)))
      (t nil))
(test car-cdr (let ((l '(137 138 139)))
                (list (cdr l) (car l)))
      ((138 139) 137))

(test catch-throw1 (let ((a 5))
                     (let ((b 6)
                           (c (catch 'here
                                (setq a 7)
                                (throw 'here 119)
                                (setq b 9))))
                       (list a b c)))
      (7 6 119))

(test catch-throw2 (let ((a 5))
                     (let ((b 6)
                           (c (catch 'here
                                (setq a 7)
                                (setq b (catch 'there
                                          (setq a (1+ a))
                                          (throw 'here 119)
                                          (throw 'there 120)
                                          (setq c "har"))))))
                       (list a b c)))
      (8 6 119))

(test cond1 (cond ((zerop 1) (print "will only print in ERROR!!!"))
                  ((null nil) '(this is it)))
      (this is it))

(test lc0 (let ((lc (list-collector)))
            (funcall lc))
      nil)
(test lc1 (let ((lc (list-collector)))
            (funcall lc 'see)
            (funcall lc 0 'evil)
            (funcall lc 'hear "no" 'evil)
            (funcall lc))
      (see 0 evil hear "no" evil))
(test lc2 (let ((lc1 (list-collector))
                (lc2 (list-collector))
                (the-evils '(evil evil)))
            (funcall lc1 'see)
            (funcall lc2 'hear)
            (funcall lc1 0)
            (funcall lc2 "no")
            (funcall lc1 (car the-evils))
            (funcall lc2 (cadr the-evils))
            (list (funcall lc1) (funcall lc2)))
      ((see 0 evil) (hear "no" evil)))

(test or0 (or) nil)
(test or1a (let ((a nil))
             (or a))
      nil)
(test or1b (let ((b 1))
             (or b))
      1)
(test or2 (let ((a nil)
                (b nil)
                (c 'a)
                (d 119))
            (or a b c d))
      a)
(test or3 (let ((a "hu")
                (b t)
                (c 'a)
                (d 119))
            (or a b c d))
      "hu")

(test and0 (and) t)
(test and1a (let ((a nil))
             (and a))
      nil)
(test and1b (let ((b 1))
             (and b))
      1)
(test and2 (let ((a nil)
                (b nil)
                (c 'a)
                (d 119))
            (and a b c d))
      nil)
(test and3 (let ((a "hu")
                (b t)
                (c 'a)
                (d 119))
            (and a b c d))
      119)

(test functionp1 (functionp 'functionp) nil)
(test functionp2 (functionp (length "functionp")) nil)
(test functionp3 (functionp "functionp") nil)
(test functionp4 (functionp #'functionp) t)
(test functionp5 (functionp #'function) t)
(test functionp6 (functionp #'defun) nil)
(test functionp7 (functionp #'car) t)

(test make-list0 (make-list 0 t) nil)
(test make-list1 (make-list 1 t) (t))
(test make-list2 (make-list 12 t) (t t t t t t t t t t t t))
(test make-list3 (make-list 3 'shnaddl) (shnaddl shnaddl shnaddl))
(test make-list4 (make-list 4 4) (4 4 4 4))
(test make-list5 (let ((n 0))
                   (make-list 7 (lambda () (prog1 n (setq n (1+ n))))))
      (0 1 2 3 4 5 6))

(test let0 (let ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
             (list a b c d e))
      (5 6 nargl nil "foo mich auch"))
(test let1 (let ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
             (let ((e b) (g e))
               (list e g)))
      (6 "foo mich auch"))
(test let2 (let (foo (bar) (bletch 'quux))
             (list foo bar bletch))
      (nil nil quux))

(test let*0 (let* ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
              (list a b c d e))
      (5 6 nargl nil "foo mich auch"))
(test let*1 (let* ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
              (let* ((e b) (g (* 3 e)))
                (list e g)))
      (6 18))
(test let*2 (let* (foo (bar) (bletch 'quux))
              (list foo bar bletch))
      (nil nil quux))

(test read0 (read) a)
(test read1 (read) 12)
(test read2 (read) "333333r")
(test read3 (read) '(3 4 5))
(test read4 (read)
      (let ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
             (let ((e b) (g e))
               (list e g))))
(test read5 (read "a") a)
(test read6 (read "12") 12)
(test read7 (read "\"333333r\"") "333333r")
(test read8 (read "'(3 4 5)") '(3 4 5))
(test read9 (read "(let ((a 5) (b (* 2 3)) (c 'nargl) d (e \"foo mich auch\"))
             (let ((e b) (g e))
               (list e g)))")
      (let ((a 5) (b (* 2 3)) (c 'nargl) d (e "foo mich auch"))
        (let ((e b) (g e))
          (list e g))))

;;; destructuring bind

(test dest-let-0 (let ()
                   23)
      23)

(test dest-let-1 (let ((a 22))
                   (+ a a))
         44)

(test dest-let-2 (let (((a b c) '(3 4 5 6)))
                   (list a b c))
      (3 4 5))

(test dest-let-3 (let (((a b c d) '(3 4 5)))
                   (list a b c d))
      (3 4 5 nil))

(test dest-let-4 (let (((a (b c) d) '(3 (4 5) 6 7)))
                   (list a b c d))
      (3 4 5 6))

;; (test dest-let-4E! (progn (errset (let (((a (b c) d) '(3 4 5 6 7)))
;;                                     (list a b c d)))
;;                           *last-error*)
;;       #/value structure error, not pair/)

(test dest-let-5 (let (((a b c . d) '(3 (4 5) 6 7 8 9)))
                   (list a b c d))
      (3 (4 5) 6 (7 8 9)))

(defun return-a-list ()
  '(3 4 5))

(test dest-let-6 (let (((a . b) (return-a-list)))
                   (list a b))
      (3 (4 5)))

;; (test dest-let-6E! (let (((a . 119) (return-a-list)))
;;                      (list a b))
;;       #/non-symbol terminates varlist/)

(test dest-let-7 (let (((a b) (return-a-list)))
                   (list a b))
      (3 4))

(test dest-let-8 (let (((a b c d) (return-a-list)))
                   (list a b c d))
      (3 4 5 nil))

(test dest-let-9 (let ((a (symbols))
                       (b '(erf trh sx thb thsdc gb th dcjn))
                       (c 1337))
                   (let (((a b c) '(3 4 5 6))
                         (b (list c c))
                         (c (* c c)))
                     (list a b c)))
      (3 (1337 1337) 1787569))

(test dest-let*-0 (let* ()
                    23)
      23)

(test dest-let*-1 (let* ((a 22))
                    (+ a a))
      44)

(test dest-let*-2 (let* (((a b c) '(3 4 5 6)))
                    (list a b c))
      (3 4 5))

(test dest-let*-3 (let* (((a b c d) '(3 4 5)))
                    (list a b c d))
      (3 4 5 nil))

(test dest-let*-4 (let* (((a (b c) d) '(3 (4 5) 6 7)))
                    (list a b c d))
      (3 4 5 6))

;; (test dest-let*-4E! (let* (((a (b c) d) '(3 4 5 6 7)))
;;                       (list a b c d))
;;       #/value structure error, not pair/)

(test dest-let*-5 (let* (((a b c . d) '(3 (4 5) 6 7 8 9)))
                    (list a b c d))
      (3 (4 5) 6 (7 8 9)))

(defun return-a-list ()
  '(3 4 5))

(test dest-let*-6 (let* (((a . b) (return-a-list)))
                    (list a b))
      (3 (4 5)))

;; (test dest-let*-6E! (let* (((a . 119) (return-a-list)))
;;                       (list a b))
;;       #/non-symbol terminates varlist/)

(test dest-let*-7 (let* (((a b) (return-a-list)))
                    (list a b))
      (3 4))

(test dest-let*-8 (let* (((a b c d) (return-a-list)))
                    (list a b c d))
      (3 4 5 nil))

(test dest-let*-9 (let* ((a (symbols))
                         (b '(fs er fgwde gr hyhye tg hyhtu))
                         (c 1337))
                    (let* (((a b c) '(3 4 5 6))
                           (b (list c c))
                           (c (* c c)))
                      (list a b c)))
      (3 (5 5) 25))

(test dest-let-10 (let (((a b (c d . e) . f) (list 'x 'y (iota 4) 8 9 10)))
                    (list a b c d e f))
      (x y 0 1 (2 3) (8 9 10)))

(test dest-let*-10
      (let* (((a b (c d . e) . f) (list 'x 'y (iota 4) 8 9 10)))
        (list a b c d e f))
      (x y 0 1 (2 3) (8 9 10)))

;; don't assign if symbol is nil

;; (test dest-nil 
;;       (let ((line "/// name set-car!\n"))
;;         (let (((nil word rest)
;;                (#r{^/// (name|impl|mina|spec|args|retv|docs) ?(.*)} line)))
;;           (list word rest)))
;;       ("name" "set-car!"))

(setq tree-counter 0)
(defun get-counter () (incf tree-counter))
      
(defun make-btree (depth)
  (if (zerop depth)
      (get-counter)
    (cons (make-btree (1- depth))  (make-btree (1- depth)))))

(setq tree-counter 0)
(setq btree (make-btree 1))

(test car (car btree) 1)
(test cdr (cdr btree) 2)


(setq tree-counter 0)
(setq btree (make-btree 2))
;; (format t "{}\n" btree)

(test caar (caar btree) 1)
(test cdar (cdar btree) 2)
(test cadr (cadr btree) 3)
(test cddr (cddr btree) 4)

(setq tree-counter 0)
(setq btree (make-btree 3))
;; (format t "{}\n" btree)

(test caaar (caaar btree) 1)
(test cdaar (cdaar btree) 2)
(test cadar (cadar btree) 3)
(test cddar (cddar btree) 4)
(test caadr (caadr btree) 5)
(test cdadr (cdadr btree) 6)
(test caddr (caddr btree) 7)
(test cdddr (cdddr btree) 8)

(setq tree-counter 0)
(setq btree (make-btree 4))
;; (format t "{}\n" btree)

(test caaaar (caaaar btree) 1)
(test cdaaar (cdaaar btree) 2)
(test cadaar (cadaar btree) 3)
(test cddaar (cddaar btree) 4)
(test caadar (caadar btree) 5)
(test cdadar (cdadar btree) 6)
(test caddar (caddar btree) 7)
(test cdddar (cdddar btree) 8)
(test caaadr (caaadr btree) 9)
(test cdaadr (cdaadr btree) 10)
(test cadadr (cadadr btree) 11)
(test cddadr (cddadr btree) 12)
(test caaddr (caaddr btree) 13)
(test cdaddr (cdaddr btree) 14)
(test cadddr (cadddr btree) 15)
(test cddddr (cddddr btree) 16)

(test div1 (/ 4) 0.25)
(test div2 (/ 4 5) 0.8)
(test div3 (/ 4 5 2) 0.4)

(test minus1 (- 3) -3)
(test minus2 (- 3 4) -1)
(test minus3 (- 3 4 5) -6)


(test reverse0 (reverse nil) nil)
(test reverse1 (reverse '(3)) (3))
(test reverse5 (reverse '(3 4 5 6 7)) (7 6 5 4 3))
(test nreverse0 (nreverse nil) nil)
(test nreverse1 (nreverse '(3)) (3))
(test nreverse5 (nreverse '(3 4 5 6 7)) (7 6 5 4 3))

(test quote0 '(3 4 5 6 7) (3 4 5 6 7))

(test dolist0 (let (result)
                (dolist (el '(3 4 5 6 7 8 9) (nreverse result))
                  (push (+ el 2) result)))
      (5 6 7 8 9 10 11))

(test dotimes1 (let ((sum 0))
                 (dotimes (i 11 sum)
                   (incf sum i)))
      55)
(test dotimes2 (let ((sum 0))
                 (dotimes (i (+ 7 4) (* sum sum))
                   (incf sum i)))
      3025)

(test dotimes11 (let ((sum 0))
                  (dotimes1 (i 10 sum)
                    (incf sum i)))
      55)
(test dotimes12 (let ((sum 0))
                  (dotimes1 (i (+ 7 3) (* sum sum))
                    (incf sum i)))
      3025)

(test <0 (< 3 3) nil)
(test <1 (< 2 3) t)
(test <2 (< -2 -3) nil)

(test >0 (> 3 3) nil)
(test >1 (> 2 3) nil)
(test >2 (> -2 -3) t)

(test <=0 (<= 3 3) t)
(test <=1 (<= 2 3) t)
(test <=2 (<= -2 -3) nil)

(test >=0 (>= 3 3) t)
(test >=1 (>= 2 3) nil)
(test >=2 (>= -2 -3) t)

(test =0 (= 3 3) t)
(test =1 (= 2 3) nil)
(test =2 (= -2 -3) nil)

;; first with defaults...
(test for1 (let ((sum 0))
             (for (i (- 2 2) (+ 5 6))
               (incf sum i))
             sum)
      55)

;; ...then without
(test for2 (let ((sum 0))
             (for (i (+ 5 5) (- 2 2) (- 1) #'>)
               (incf sum i))
             sum)
      55)

(test errset0 (errset (car 'foo) nil) nil)
(test errset1 (errset (car '(foo)) nil) (foo))
(test errset-p0 (progn (princ "should see error: yes\n")
                       (errset (error "this is a user error")))
      nil)
(test errset-p1 (progn (princ "should see error: yes\n")
                       (errset (error "this is a user error") t))
      nil)
(test errset-p2 (progn (princ "should see error: no\n")
                       (errset (error "this is a user error") nil))
      nil)
(test errset-p3 (progn (princ "should see error: yes\n")
                       (errset (error "this is a user error") 0))
      nil)

(test symbol-function0 (string-match "^#<builtin function \\(car PAIR\\) "
                                     (string (symbol-function 'car)))
      ("#<builtin function (car PAIR) "))

(test symbol-function1 (progn (errset (symbol-function 'this-is-unbound) nil)
                              (error-message *last-error*))
      "symbol this-is-unbound has no function value")

(test join0 (join " -- " '()) "")
(test join1 (join " -- " '(huhu)) "huhu")
(test join2 (join " -- " '(huhu "lala")) "huhu -- lala")
(test join3 (join " -- " '(huhu "lala" 123)) "huhu -- lala -- 123")


(test setf-car-1 (let ((l '(d e f g h i j)))
                   (setf (car l) 119)
                   l)
      (119 e f g h i j))
(test setf-car-1-ex (macroexpand '(setf (car l) 119))
      (rplaca-ret-value l 119))
(test setf-car-2 (let ((l '(d e f g h i j)))
                   (setf (car (cdr l)) 119)
                   l)
      (d 119 f g h i j))
(test setf-car-2-ex (macroexpand '(setf (car (cdr l)) 119))
      (rplaca-ret-value (cdr l) 119))
(test setf-car-3 (let ((l '(d e f g h i j)))
                   (setf (car (cdr (cdr l))) 119)
                   l)
      (d e 119 g h i j))
(test setf-car-3-ex (macroexpand '(setf (car (cdr (cdr l))) 119))
      (rplaca-ret-value (cdr (cdr l)) 119))
(test setf-car-4 (let ((l '((d e f) g h i j)))
                   (setf (car (cdr (car l))) 119)
                   l)
      ((d 119 f) g h i j))
(test setf-car-4-ex (macroexpand '(setf (car (cdr (car l))) 119))
      (rplaca-ret-value (cdr (car l)) 119))

(test setf-cdr-1 (let ((l '(d e f g h i j)))
                   (setf (cdr l) 119)
                   l)
      (d . 119))
(test setf-cdr-1-ex (macroexpand '(setf (cdr l) 119))
      (rplacd-ret-value l 119))
(test setf-cdr-2 (let ((l '(d e f g h i j)))
                   (setf (cdr (cdr l)) 119)
                   l)
      (d e . 119))
(test setf-cdr-2-ex (macroexpand '(setf (cdr (cdr l)) 119))
      (rplacd-ret-value (cdr l) 119))
(test setf-cdr-3 (let ((l '(d e f g h i j)))
                   (setf (cdr (cdr (cdr l))) 119)
                   l)
      (d e f . 119))
(test setf-cdr-3-ex (macroexpand '(setf (cdr (cdr (cdr l))) 119))
      (rplacd-ret-value (cdr (cdr l)) 119))
(test setf-cdr-4 (let ((l '((d e f) g h i j)))
                   (setf (cdr (cdr (car l))) 119)
                   l)
      ((d e . 119) g h i j))
(test setf-cdr-4-ex (macroexpand '(setf (cdr (cdr (car l))) 119))
      (rplacd-ret-value (cdr (car l)) 119))

(setq tt '((aa . da) .
           (ad . dd)))

(test setf-caar (progn (setf (caar tt) 71) tt)
      ((71 . da) .
       (ad . dd)))
(test setf-cadr (progn (setf (cadr tt) 72) tt)
      ((71 . da) .
       (72 . dd)))
(test setf-cdar (progn (setf (cdar tt) 73) tt)
      ((71 . 73) .
       (72 . dd)))
(test setf-cddr (progn (setf (cddr tt) 74) tt)
      ((71 . 73) .
       (72 . 74)))

(defparameter ttt '(((aaa . daa) .
                     (ada . dda)) .
                     ((aad . dad) .
                      (add . ddd))))

(test setf-caaar (progn (setf (caaar ttt) 34) ttt)
      (((34 . daa) .
        (ada . dda)) .
        ((aad . dad) .
         (add . ddd))))
(test setf-caadr (progn (setf (caadr ttt) 35) ttt)
      (((34 . daa) .
        (ada . dda)) .
        ((35 . dad) .
         (add . ddd))))
(test setf-cadar (progn (setf (cadar ttt) 36) ttt)
      (((34 . daa) .
        (36 . dda)) .
        ((35 . dad) .
         (add . ddd))))
(test setf-caddr (progn (setf (caddr ttt) 37) ttt)
      (((34 . daa) .
        (36 . dda)) .
        ((35 . dad) .
         (37 . ddd))))
(test setf-cdaar (progn (setf (cdaar ttt) 38) ttt)
      (((34 . 38) .
        (36 . dda)) .
        ((35 . dad) .
         (37 . ddd))))
(test setf-cdadr (progn (setf (cdadr ttt) 39) ttt)
      (((34 . 38) .
        (36 . dda)) .
        ((35 . 39) .
         (37 . ddd))))
(test setf-cddar (progn (setf (cddar ttt) 40) ttt)
      (((34 . 38) .
        (36 . 40)) .
        ((35 . 39) .
         (37 . ddd))))
(test setf-cdddr (progn (setf (cdddr ttt) 41) ttt)
      (((34 . 38) .
        (36 . 40)) .
        ((35 . 39) .
         (37 . 41))))

(setq tttt '((((aaaa . daaa) .
               (adaa . ddaa)) .
               ((aada . dada) .
                (adda . ddda))) .
                (((aaad . daad) .
                  (adad . ddad)) .
                  ((aadd . dadd) .
                   (addd . dddd)))))

(test setf-caaaar (progn (setf (caaaar tttt) 119) tttt)
      ((((119 . daaa) .
         (adaa . ddaa)) .
         ((aada . dada) .
          (adda . ddda))) .
          (((aaad . daad) .
            (adad . ddad)) .
            ((aadd . dadd) .
             (addd . dddd)))))
(test setf-caaadr (progn (setf (caaadr tttt) 120) tttt)
      ((((119 . daaa) .
         (adaa . ddaa)) .
         ((aada . dada) .
          (adda . ddda))) .
          (((120 . daad) .
            (adad . ddad)) .
            ((aadd . dadd) .
             (addd . dddd)))))
(test setf-caadar (progn (setf (caadar tttt) 121) tttt)
      ((((119 . daaa) .
         (adaa . ddaa)) .
         ((121 . dada) .
          (adda . ddda))) .
          (((120 . daad) .
            (adad . ddad)) .
            ((aadd . dadd) .
             (addd . dddd)))))
(test setf-caaddr (progn (setf (caaddr tttt) 122) tttt)
      ((((119 . daaa) .
         (adaa . ddaa)) .
         ((121 . dada) .
          (adda . ddda))) .
          (((120 . daad) .
            (adad . ddad)) .
            ((122 . dadd) .
             (addd . dddd)))))
(test setf-cadaar (progn (setf (cadaar tttt) 123) tttt)
      ((((119 . daaa) .
         (123 . ddaa)) .
         ((121 . dada) .
          (adda . ddda))) .
          (((120 . daad) .
            (adad . ddad)) .
            ((122 . dadd) .
             (addd . dddd)))))
(test setf-cadadr (progn (setf (cadadr tttt) 124) tttt)
      ((((119 . daaa) .
         (123 . ddaa)) .
         ((121 . dada) .
          (adda . ddda))) .
          (((120 . daad) .
            (124 . ddad)) .
            ((122 . dadd) .
             (addd . dddd)))))
(test setf-caddar (progn (setf (caddar tttt) 125) tttt)
      ((((119 . daaa) .
         (123 . ddaa)) .
         ((121 . dada) .
          (125 . ddda))) .
          (((120 . daad) .
            (124 . ddad)) .
            ((122 . dadd) .
             (addd . dddd)))))
(test setf-cadddr (progn (setf (cadddr tttt) 126) tttt)
      ((((119 . daaa) .
         (123 . ddaa)) .
         ((121 . dada) .
          (125 . ddda))) .
          (((120 . daad) .
            (124 . ddad)) .
            ((122 . dadd) .
             (126 . dddd)))))
(test setf-cdaaar (progn (setf (cdaaar tttt) 127) tttt)
      ((((119 . 127) .
         (123 . ddaa)) .
         ((121 . dada) .
          (125 . ddda))) .
          (((120 . daad) .
            (124 . ddad)) .
            ((122 . dadd) .
             (126 . dddd)))))
(test setf-cdaadr (progn (setf (cdaadr tttt) 128) tttt)
      ((((119 . 127) .
         (123 . ddaa)) .
         ((121 . dada) .
          (125 . ddda))) .
          (((120 . 128) .
            (124 . ddad)) .
            ((122 . dadd) .
             (126 . dddd)))))
(test setf-cdadar (progn (setf (cdadar tttt) 129) tttt)
      ((((119 . 127) .
         (123 . ddaa)) .
         ((121 . 129) .
          (125 . ddda))) .
          (((120 . 128) .
            (124 . ddad)) .
            ((122 . dadd) .
             (126 . dddd)))))
(test setf-cdaddr (progn (setf (cdaddr tttt) 130) tttt)
      ((((119 . 127) .
         (123 . ddaa)) .
         ((121 . 129) .
          (125 . ddda))) .
          (((120 . 128) .
            (124 . ddad)) .
            ((122 . 130) .
             (126 . dddd)))))
(test setf-cddaar (progn (setf (cddaar tttt) 131) tttt)
      ((((119 . 127) .
         (123 . 131)) .
         ((121 . 129) .
          (125 . ddda))) .
          (((120 . 128) .
            (124 . ddad)) .
            ((122 . 130) .
             (126 . dddd)))))
(test setf-cddadr (progn (setf (cddadr tttt) 132) tttt)
      ((((119 . 127) .
         (123 . 131)) .
         ((121 . 129) .
          (125 . ddda))) .
          (((120 . 128) .
            (124 . 132)) .
            ((122 . 130) .
             (126 . dddd)))))
(test setf-cdddar (progn (setf (cdddar tttt) 133) tttt)
      ((((119 . 127) .
         (123 . 131)) .
         ((121 . 129) .
          (125 . 133))) .
          (((120 . 128) .
            (124 . 132)) .
            ((122 . 130) .
             (126 . dddd)))))
(test setf-cddddr (progn (setf (cddddr tttt) 134) tttt)
      ((((119 . 127) .
         (123 . 131)) .
         ((121 . 129) .
          (125 . 133))) .
          (((120 . 128) .
            (124 . 132)) .
            ((122 . 130) .
             (126 . 134)))))

(test setf-elt-L (let ((l '(22 33 44 55 66)))
                   (setf (elt l 2) 'bb)
                   l)
      (22 33 bb 55 66))
;; (test-is "setf elt S" (let ((s "abcdefg"))
;;                         (setf (elt s 2) #\4)
;;                         s)
;;          "ab4defg")
;; (test-is "setf elt !S" (progn (errset (let ((s "abcdefg"))
;;                                         (setf (elt s 2) 4)
;;                                         s)
;;                                       nil)
;;                               sys:last-error)
;;          "setelt: cannot set string element to object type number")

(defun square (n) (* n n))
(test setf-symbol-function (string (progn (setf (symbol-function '^2) #'square)
                                          #'^2))
      "#<function square (n)>")

(setq symbol 'prop-bearer)
(test setf-get (progn (setf (get symbol 'shlongity) 1e19)
                      (get symbol 'shlongity))
      1e19)

(defparameter shnllli)
(test setf-symbol-value (progn (setf (symbol-value 'shnllli) 13.4)
                               shnllli)
      13.4)

(test filter (filter (lambda (n) (> n 15)) '(9 15 18 12 21 6 3 24))
      (18 21 24))

;; from the CLHS setf page

(test setf-clhs (progn (setq x (cons 'a 'b))
                       (setq y (list 1 2 3))
                       (setf (car x) 'x)
                       (setf (cadr y) (car x))
                       (setf (cdr x) y)
                       (cons x y))
      ((x 1 x 3) . (1 x 3)))

(setf b '(3 4 5 6 7))
;; (test incf0 (progn (incf (car b) 2)
;;                   b)
;;      (5 4 5 6 7))

;; (test decf0 (progn (decf (cddr b))
;;                    b)
;;       ((3) (4) ((4 8))))

(defparameter b '((3) (4) ((5 6))))
;; (test incf1 (progn (incf (car (cdr (caaddr b))) 2)
;;                   b)
;;       ((3) (4) ((5 8))))

;; (test decf1 (progn (decf (car (caaddr b)))
;;                   b)
;;       ((3) (4) ((4 8))))

;; (test incf2 (progn (incf (car (cdr (caaddr b))) 2)
;;                   b)
;;       ((3) (4) ((5 8))))

;; (test decf2 (progn (decf (car (caaddr b)))
;;                   b)
;;       ((3) (4) ((4 8))))

;; (test pop (cons (pop (caaddr b)) b)
;;       (4 (3) (4) ((8))))

;; (test pushf-1 (progn (push '73 (caaddr b))
;;                      b)
;;       ((3) (4) ((73 8))))

;; (test pushf-2 (progn (push '73 (car (car (cdr (cdr b)))))
;;                      b)
;;       ((3) (4) ((73 73 8))))

;;; from CLHS

(defun truncate (x)
  (read (string-subst "\\..*" "" (string x))))

(defun middleguy (x) (nth (truncate (/ (1- (list-length x)) 2)) x))
(defun set-middleguy (x v)
  (unless (null x)
    (rplaca (nthcdr (truncate (/ (1- (list-length x)) 2)) x) v))
  v)

(defsetf middleguy set-middleguy)
(defparameter a (list 'a 'b 'c 'd))
(defparameter b (list 'x))
(defparameter c (list 1 2 3 (list 4 5 6) 7 8 9))

(test defsetf-clhs-1 (cons (setf (middleguy a) 3)
                           a)
      (3 a 3 c d))
(test defsetf-clhs-2 (cons (setf (middleguy b) 7)
                           b)
      (7 7))
(test defsetf-clhs-3 (cons (setf (middleguy (middleguy c))
                                 'middleguy-symbol)
                           c)
      (middleguy-symbol 1 2 3 (4 middleguy-symbol 6) 7 8 9))

(test setf-nth (let ((l '(a b c d e f g h)))
                 (setf (nth 3 l) 133)
                 l)
      (a b c 133 e f g h))

;; End Of Tests ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(format t "\ndone testing")
(if fails
    (progn
      (format t "\n\n")
      (let ((list (reverse fails)))
        (format t "{} FAILS:\n" (length fails))
        (while list
          (format t "  {}\n" (pop list)))))
  (format t ", all ok\n\n"))
(when dup-test-names
    (dolist (testname dup-test-names)
      (format t "WARN: duplicate test name {}\n" testname))
    (terpri))
