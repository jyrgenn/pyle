# This file is part of the Python Lambda Experiment (pyle),
# Copyright (C) 2017, 2018 Juergen Nickelsen <ni@w21.org>.
# See LICENSE for the conditions under which this code is licensed to you.

import re

import dbg
from utils import *
from exceptions import *
import dep
import utils

# Symbols that we need internally. These need to be created ASAP, i.e. as soon
# as we have the root environment.
Nil = None
NilName = "nil"
T = None
TName = "t"
NamePropSymbol = None
NamePropSymbolName = "sys:name"
ImmutableSymbol = None
ImmutableSymbolName = "sys:immutable"
FunctionSymbol = None
FunctionSymbolName = "sys:function"

RootEnv = None
CurrentEnv = None


def p2l(value):
    """Return an Object for value.

    If value is already an Object, return it; otherwise, create the appropriate
    Object and return it.

    """
    if isinstance(value, Object):
        return value
    elif type(value) is bool:
        return T if value else Nil
    elif type(value) in (int, float):
        return Number(value)
    elif type(value) is str:
        return String(value)
    elif type(value) is list:
        lc = ListCollector()
        for thing in value:
            lc.add(p2l(thing))
        return lc.list()
    elif type(value) is dict:
        lc = ListCollector()
        for key, val in value.items():
            if type(key) is str:
                key = intern(key)
            lc.add(cons(p2l(key), p2l(val)))
        return lc.list()
    else:
        return String(str(value))


class Object:
    """This is the base class for all Pyle objects, purely abstract.
    
    There are a few default functions.
    
    """
    
    def __repr__(self):
        """Return a default string representation."""
        return "#<{n}:{i}>".format(n=self.__class__.__name__, i=id(self))

    def __str__(self):
        """Return the nicely formatted string for object."""
        return self.__repr__()
    
    def __iter__(self):
        """Return an iterator for the object. Will raise an error for most."""
        return ListIterator(self)
    
    def __bool__(self):
        """Return the boolean value of the object."""
        return self is not Nil
    
    def __format__(self, format_spec):
        """Return a formatted representation of the object."""
        return str(self).__format__(format_spec)
    
    def eq(self, obj):
        """Return true iff object is eq to obj."""
        return self is obj
    
    def type(self):
        """Return the class name of object."""
        return self.__class__.__name__
    
    def isNil(self):
        """Return true iff the object is Nil."""
        return self is Nil
    
    def isProper(self):
        """Return true iff the object is a proper list."""
        return False
    
    def dump(self):
        """Return a text dump of the object."""
        return self.__repr__()
    
    def cxr(self):
        """Return car and cdr of the object. Will be an error for most."""
        raise PyleIterateAtomError(self)
    
    def typename(self):
        """Return the lowercase class name of object."""
        return self.__class__.__name__.lower()
    
    def describe(self):
        """Return object's attributes as a dict."""
        return { "type": intern(self.typename()), "id": id(self) }
    
    def car(self):
        """Return the car of a cons.

        If this is not a cons => error.
        """
        raise PyleArgTypeError(self, 'car', 'list')
    
    def cdr(self):
        """Return the cdr of a cons.

        If this is not a cons => error.
        """
        raise PyleArgTypeError(self, 'cdr', 'list')
    
    def rplaca(self, ob):
        """Replace the car of a cons.

        If this is not a cons => error.
        """
        raise PyleArgTypeError(self, 'rplaca', 'pair')
    
    def rplacd(self, ob):
        """Replace the cdr of a cons.

        If this is not a cons => error.
        """
        raise PyleArgTypeError(self, 'rplacd', 'pair')
    
    def isAtom(self):
        """Return true iff the object is an Atom. False by default."""
        return False
    
    def isEnvironment(self):
        """Return true iff this is an environment. False by default."""
        return False
    
    def isFunction(self):
        """Return true iff this is a function. False by default."""
        return False
    
    def isList(self):
        """Return true iff this is a list. False by default."""
        return False
    
    def isMacro(self):
        """Return true iff this is a macro. False by default."""
        return False
    
    def isNumber(self):
        """Return true iff this is a number. False by default."""
        return False
    
    def isPair(self):
        """Return true iff this is a pair. False by default."""
        return False
    
    def isString(self):
        """Return true iff this is a string. False by default."""
        return False
    
    def isSymbol(self):
        """Return true iff this is a symbol. False by default."""
        return False
    
    def isError(self):
        """Return true iff this is an error. False by default."""
        return False


class Error(Object):
    """Error class. These are derived from the Pyle exceptions."""
    
    def __init__(self, e):
        self.e = e              # the Python exception
        errName = re.sub("^Pyle", "", e.__class__.__name__)
        self.e.kind = intern(utils.dashify(errName))
        
    def isError(self):
        return True
    
    def __str__(self):
        elems = ["#<Error"]
        firstSem = ""
        for key, value in self.e.__dict__.items():
            elems.append("{f} {k}:{v}".format(f=firstSem, k=key, v=value))
            firstSem = ";"
        elems.append(">")
        return "".join(elems)
    
    def __repr__(self):
        return str(self)
    
    def describe(self):
        attrs = super().describe()
        attrs.update(self.e.__dict__)
        return attrs
    
_gensym_counter = 8385

class Symbol(Object):
    def gensym(prefix):
        global _gensym_counter
        while True:
            _gensym_counter += 1
            name = prefix + str(_gensym_counter)
            if name not in symbolTable:
                return Symbol(name)
        
    def __init__(self, name, immutable=False, selfval=False):
        # dbg.p(dbg.sym, "init Symbol({n}, imm={i}, self={s})".
        #       format(n=name, i=immutable, s=selfval))
        if name == NamePropSymbolName:
            namesym = self
        else:
            namesym = NamePropSymbol
        self._props = { namesym: String(name) }
        if selfval:
            self.defvar(self)
        if immutable:
            self._props[ImmutableSymbol] = immutable
    def immutable(self): 
        return self._props.get(ImmutableSymbol, None)
    def makeImmutable(self):
        self.putprop(ImmutableSymbol, T)
    def name(self):
        return self._props[NamePropSymbol]
    def isAtom(self):
        return True
    def isProper(self):
        return self is Nil
    def isList(self):
        return self is Nil
    def __str__(self):
        return self._props[NamePropSymbol].strValue()
    def __repr__(self):
        return str(self)
    def dump(self):
        return "#<{n}:{s}>".format(n=self.__class__.__name__, s=self.describe())
    def getprop(self, prop):
        return self._props.get(prop, None)
    def putprop(self, prop, value):
        if value is Nil and prop in self._props:
            del self._props[prop]
        else:
            self._props[prop] = value
    def props(self):
        return self._props
    def isSymbol(self):
        return True
    def defvar(self, ob):
        envbind(self, ob, RootEnv)
    def bind(self, ob):
        envbind(self, ob)
    def boundp(self):
        # dbg.p(dbg.sym, "boundp", self, CurrentEnv.getvalOrNone(self))
        return CurrentEnv.getvalOrNone(self) is not None
    def value(self):
        return envget(self)
    def setFunction(self, func):
        self.putprop(FunctionSymbol, func)
    def getFunction(self):
        return self.getprop(FunctionSymbol)
    def equal(self, ob):
        return self is ob
    def length(self):
        """Return the length of the proper list.
        It is an error if the argument is not a proper list.
        """
        if self is Nil:
            return 0
        else:
            raise PyleArgTypeError(self, 'length', 'list')
    def lengthI(self):
        return 0
    def elt(self):
        if self is Nil:
            return Nil
        else:
            raise PyleArgTypeError(self, 'elt', 'list')
    def car(self):
        if self is Nil:
            return Nil
        else:
            raise PyleArgTypeError(self, 'car', 'list')
    def cdr(self):
        if self is Nil:
            return Nil
        else:
            raise PyleArgTypeError(self, 'cdr', 'list')
    def cxr(self):
        if self is Nil:
            return Nil, Nil
        else:
            raise PyleArgTypeError(self, 'cxr', 'list')
    def describe(self):
        attrs = super().describe()
        attrs["props"] = self._props
        return attrs


def init_symbol():
    global Nil, T, ImmutableSymbol, FunctionSymbol, NamePropSymbol, IdPropSymbol
    # NamePropSymbol *must* be defined first, so we can name the other ones
    NamePropSymbol = intern(NamePropSymbolName)
    ImmutableSymbol = intern(ImmutableSymbolName)
    FunctionSymbol = intern(FunctionSymbolName)
    Nil = internSelf(NilName)
    T = internSelf(TName)

class Function(Object):
    def call(self, arglist, nargs):
        raise PyleNotImplementedError(self, "call")
    def checkArgs(self, nargs):
        if nargs < self.minargs:
            raise PyleArgCountError(self, nargs, self.minargs)
        if self.maxargs and nargs > self.maxargs:
            raise PyleArgCountError(self, nargs, self.maxargs)
    def isFunction(self):
        return True
    def isSpecial(self):
        return self.special
    def docstring(self):
        return self.synopsis() + "\n" + self._docstring
    def synopsis(self):
        return self.typename() + " " + str(cons(self.name, self.params))
    def setName(self, name):
        self.name = name


class Macro(Function):
    def __init__(self, name, params, body):
        self.name = name
        self.params = params
        self.minargs = params.lengthI()
        self.maxargs = self.minargs if params.isProper() else None
        # dbg.p(dbg.form, name, "has minargs", self.minargs)
        self.special = False

        maybeDocstring, restBody = body.cxr()
        if maybeDocstring.isString():
            self._docstring = maybeDocstring.strValue()
            self.body = restBody
        else:
            self._docstring = ""
            self.body = body
    def __str__(self):
        return "#<macro {n} {p}>".\
          format(n=self.name, p="()" if self.params is Nil else self.params) 
    def __repr__(self):
        return self.__str__()
    def isMacro(self):
        return True
    def call(self, arglist, nargs):
        raise PyleMacroCallError(self.name)
    def expand(self, arglist):
        self.checkArgs(arglist.length())
        savedEnv = enterEnvironment()
        try:
            bind_params(self.params, arglist, self.name)
            # dbg.p(dbg.call, "evalProgn in", CurrentEnv, self.body)
            return dep.evalProgn(self.body)
        finally:
            backtoEnvironment(savedEnv)
    def describe(self):
        attrs = super().describe()
        attrs.update({
            "name": self.name,
            "params": self.params,
            "minargs": Number(self.minargs),
            "maxargs": Number(self.maxargs),
            "synopsis": self.synopsis(),
            "docstring": self._docstring,
            "body": self.body, 
        })
        return attrs


# this is used for parameter bindings on function calls and variable
# bindings in let/let*; destructuring binds will go here, too
def bind_params(params, args, function=None, needEval=False, strict=True):
    # dbg.p(dbg.bind, "bind_params:", params, args, function, needEval)
    nargs = args.length() if args.isList() else 1
    while params.isPair() and args.isPair():
        param, params = params.cxr()
        arg, args = args.cxr()
        if needEval:        # need to eval this even if param is
                            # nil, but only once, on the top level,
                            # not on the recursive descent
            arg = dep.Eval(arg)
        if param.isSymbol():
            if param is Nil:
                continue    # skip bindings if param symbol is nil
            # dbg.p(dbg.bind, "envbind:", param, arg)
            envbind(param, arg)
        elif param.isPair():
            bind_params(param, arg, function, False, False)
    # one of them being non-nil can only happen when binding for a
    # function call, not with let/let*
    if args is Nil:
        if params is Nil:
            return          # standard situation, proper params
                            # list, also with let and let*
        elif params.isSymbol():
            # dbg.p(dbg.bind, "envbind:", params, Nil)
            envbind(params, Nil)
        elif strict and function:
            raise PyleArgCountError(function, nargs, function.minargs)
        else:
            for param in params:
                envbind(param, Nil)
    else:
        if params is Nil:
            if function:
                raise PyleArgCountError(function, nargs, function.maxargs)
            # if we do not function parameter, but let/let*
            # bindings, it is fine if we have more arguments than
            # parameters; we do still have to eval them in case,
            # though
            if needEval:
                if args.isPair():
                    dep.evalLis(args)
        elif params.isSymbol():
            # bind rest of args to symbol, which is the end of an
            # improper arameter list
            if needEval:
                args, _ = dep.evalList(args)
            # dbg.p(dbg.bind, "envbind:", params, args)
            envbind(params, args)
        else:
            assert False, "binding wtf 3? {f}: params {p}, args left {a}".\
                format(f=function, a=args, p=params)

# Form follows Function (nyah, nyah, nyah...)
class Form(Function):
    def __init__(self, name, params, body):
        self.env = CurrentEnv
        self.name = name
        self.params = params
        self.minargs = params.lengthI()
        self.maxargs = self.minargs if params.isProper() else None
        # dbg.p(dbg.form, name, "has minargs", self.minargs)
        self.special = False

        maybeDocstring, restBody = body.cxr()
        if maybeDocstring.isString():
            self._docstring = maybeDocstring.strValue()
            self.body = restBody
        else:
            self._docstring = ""
            self.body = body

    def __str__(self):
        return "#<function {n} {p}>".\
          format(n=self.name, p="()" if self.params is Nil else self.params) 
    def __repr__(self):
        return self.__str__()
    def dump(self):
        return self.__str__()
    def call(self, arglist, nargs):
        self.checkArgs(nargs)
        savedEnv = enterEnvironment(self.env)
        try:
            bind_params(self.params, arglist)
            # dbg.p(dbg.call, "evalProgn in", CurrentEnv, self.body)
            return dep.evalProgn(self.body)
        finally:
            backtoEnvironment(savedEnv)
    def describe(self):
        attrs = super().describe()
        attrs.update({
            "env": self.env,
            "name": self.name,
            "params": self.params,
            "minargs": Number(self.minargs),
            "maxargs": Number(self.maxargs),
            "synopsis": self.synopsis(),
            "special": p2l(self.special),
            "docstring": self._docstring,
            "body": self.body, 
        })
        return attrs
        
class Builtin(Function):
    def __init__(self, name, code, minargs, maxargs, special):
        self.name = name
        self.code = code
        self.minargs = minargs
        self.maxargs = maxargs
        self.special = special
        doclines = code.__doc__.splitlines()
        self._docstring = "\n".join(doclines[1:])
        self._synopsis = doclines[0]
    def call(self, arglist, nargs):
        self.checkArgs(nargs)
        # dbg.p(dbg.call, "call {s} with {a} [{n}]".
        #       format(s=self, a=arglist, n=nargs))
        return self.code(arglist)
    def __str__(self):
        return "#<{s}>".format(s=self.synopsis())
    def __repr__(self):
        return self.__str__()
    def dump(self):
        return self.__str__()
    def synopsis(self):
        return self._synopsis
    def describe(self):
        attrs = super().describe()
        attrs.update({
            "name": self.name,
            "minargs": Number(self.minargs),
            "maxargs": Number(self.maxargs),
            "synopsis": self._synopsis,
            "special": p2l(self.special),
            "docstring": self._docstring,
        })
        return attrs

envcount = 0

class Environment(Object):
    def __init__(self, parent=None):
        global envcount
        self._count = envcount
        envcount += 1
        if parent:
            self._level = parent._level + 1
            self._parent = parent
        else:
            self._level = 0
            self._parent = Nil
        self._map = {}
    def __str__(self):
        s = "env{c}[{l}]".format(c=self._count, l=self._level)
        if self._level > 0:
            s += ":" + str(self._map)
        return s
    def isEnvironment(self):
        return True
    def getvalOrNone(self, symbol):
        """Return the value of the symbol in this environment or another.
        If there is no value, return None.
        """
        env = self
        while env:
            if symbol in env._map:
                return env._map[symbol]
            env = env._parent
        return None
    def getval(self, symbol):
        val = self.getvalOrNone(symbol)
        if val is None:
            raise PyleUnboundVarError(symbol)
        else:
            return val
    def bind(self, symbol, value):
        """Bind a value in this environment."""
        self._map[symbol] = value
    def setval(self, symbol, value):
        """Set the value of the symbol in its current binding. That binding
        may be in another environment than this one, if this is not
        the root environment.
        """
        current = self
        while current:
            if symbol in current._map or current == RootEnv:
                current._map[symbol] = value
                return
            current = current._parent
        assert False, "RootEnv not at end of env chain (never happens)"
    def level(self):
        return self._level
    def describe(self):
        attrs = super().describe()
        attrs.update({
            "level": self._level,
            "parent": self._parent,
            "vars": p2l(self._map),
        })
        return attrs

symbolTable = {}

def enterEnvironment(parent=None):
    global CurrentEnv
    if parent is None:
        parent = CurrentEnv
    savedEnv = CurrentEnv
    CurrentEnv = Environment(parent)
    # dbg.p(dbg.env, "enter environment", CurrentEnv,
    #       "saved", savedEnv, "parent", parent)
    return savedEnv

def backtoEnvironment(env):
    global CurrentEnv
    assert CurrentEnv != RootEnv
    # dbg.p(dbg.env, "drop environment", CurrentEnv, "back to", env)
    CurrentEnv = env

def envbind(symbol, value, env=None):
    if not env:
        env = CurrentEnv
    if symbol.immutable():
        raise PyleModifyImmutableError(symbol)
    # dbg.p(dbg.env, "bind {s} <= {v}".format(s=symbol, v=value))
    env.bind(symbol, value)

def envget(symbol, env=None):
    global CurrentEnv
    if not env:
        env = CurrentEnv
    # dbg.p(dbg.env, "envget", symbol, env)
    return env.getval(symbol)

def envset(symbol, value, env=None):
    # dbg.p(dbg.env, "envset", symbol, value, env)
    if env is None:
        env = CurrentEnv
    if symbol.immutable():
        raise PyleModifyImmutableError(eymbol)
    env.setval(symbol, value)

def intern(name, immutable=False, selfval=False):
    assert str(name) != "None"
    if name in symbolTable:
        sym = symbolTable[name]
        # dbg.p(dbg.sym, "intern", name, "has", sym)
    else:
        sym = Symbol(name, immutable, selfval)
        symbolTable[name] = sym
        # dbg.p(dbg.sym, "intern new", sym)
    return sym

def internSelf(name):
    return intern(name, True, True)

class Number(Object):
    def __init__(self, value):
        self._value = value
    def __str__(self):
        return str(self._value)
    def __repr__(self):
        return self.__str__()
    def __format__(self, format_spec):
        return self._value.__format__(format_spec)
    def dump(self):
        return "#<{n}:{s}>".format(n=self.__class__.__name__, s=self)
    def isAtom(self):
        return True
    def isNumber(self):
        return True
    def eq(self, ob):
        return ob.isNumber() and self._value == ob._value
    def equal(self, ob):
        self.eq(ob)
    def numValue(self):
        return self._value
    def describe(self):
        attrs = super().describe()
        attrs["value"] = self
        return attrs

escapeSpecials = {
    "\a": "\\a", "\b": "\\b", "\f": "\\f", "\n": "\\n",
    "\r": "\\r", "\t": "\\t", "\v": "\\v", "\"": "\\\"", "\\": "\\\\"
}

class String(Object):
    def __init__(self, value):
        self._value = str(value)
    def __str__(self):
        return self._value
    def __repr__(self):
        chars = ['"']
        for c in self._value:
            chars.append(escapeSpecials.get(c, c))
        chars.append('"')
        return "".join(chars)
    def dump(self):
        return "#<{n}{i}:{s}>".\
            format(n=self.__class__.__name__, i=id(self), s=repr(self))
    def isString(self):
        return True
    def isAtom(self):
        return True
    def eq(self, ob):
        result = ob.isString() and self._value == ob._value
        # print("eq S {s} id {i1}, {o} id {i2} = {r}".
        #       format(s=self.dump(), i1=id(self), o=ob.dump(),
        #              i2=id(ob), r=result))
        return result
    def equal(self, ob):
        self.eq(ob)
    def strValue(self):
        return self._value
    def describe(self):
        attrs = super().describe()
        attrs["value"] = self
        return attrs

class Pair(Object):
    def __init__(self, car, cdr):
        self._car = car
        self._cdr = cdr
    def dump(self):
        return "#<{n}:{s}>".format(n=self.__class__.__name__, s=self)
    def __repr__(self, obset=None):
        if obset is None:
            # print("start anew with pair", id(self))
            obset = set()
        if self in obset:
            # print("already seen pair", id(self))
            return "<...>"
        obset.add(self)
        result = ["("]
        current = self
        first = True
        while current.isPair():
            if first:
                first = False
            else:
                result.append(" ")
            el, current = current.cxr()
            if el.isPair():
                result.append(el.__repr__(obset))
            else:
                result.append(repr(el))
        if current is not Nil:
            result.append(" . ")
            result.append(repr(current))
        result.append(")")
        return "".join(result)
    def __str__(self):
        return repr(self)
    def isAtom(self):
        return False
    def isProper(self):
        return self._cdr.isProper()
    def length(self):
        len = 0
        for _ in self:
            len += 1
        return len
    def lengthI(self):
        """For improper lists, too."""
        len = 0
        while self.isPair():
            self = self._cdr
            len += 1
        return len
    def isPair(self):
        return True
    def car(self):
        return self._car
    def cdr(self):
        return self._cdr
    def cxr(self):
        return self._car, self._cdr
    def rplaca(self, newcar):
        self._car = newcar
        return self
    def rplacd(self, newcdr):
        self._cdr = newcdr
        return self
    def equal(self, ob):
        if self.type() != ob.type():
            return False
        return self._car.equal(ob._car) and self._cdr.equal(ob._cdr)
    def isList(self):
        return True

class ListIterator():
    def __init__(self, ob):
        if not ob.isList():
            raise PyleIterateTypeError(ob)
        self.list = ob
    def __iter__(self):
        return self
    def __next__(self):
        if self.list.isNil():
            raise StopIteration()
        ob, self.list = self.list.cxr()
        return ob    

class ListCollector():
    def __init__(self):
        self._first = Nil
        self._last = Nil
        # dbg.p(dbg.lc, self, "initialized with", self._first, self._last)
    def __repr__(self):
        return "#<{n} {l}>".format(n=self.__class__.__name__, l=self._first)
    def add(self, ob):
        # dbg.p(dbg.lc, "lc gets", ob)
        newcons = cons(ob, Nil)
        if self._first is Nil:
            self._first = newcons
        else:
            # check that list ends with Nil, meaning not something else has
            # been put at the end through addEnd()
            assert self._last._cdr is Nil, \
                "self._last._cdr is {0}".format(self._last._cdr)
            self._last.rplacd(newcons)
        self._last = newcons
        # dbg.p(dbg.lc, "lc now has", self._first)
    def addEnd(self, ob):
        if self._first:
            self._last.rplacd(ob)
        else:
            self._first = ob
    def list(self):
        # dbg.p(dbg.lc, "lc returns", self._first)
        return self._first
    def last(self):
        return self._last
    
def cons(car, cdr):
    return Pair(car, cdr)

def list(*args):
    lc = ListCollector()
    for elem in args:
        lc.add(elem)
    return lc.list()

def init_env():
    # dbg.p(dbg.env, "init env")
    global RootEnv
    RootEnv = Environment()
    global CurrentEnv
    CurrentEnv = RootEnv

if Nil is None:
    init_env()
    init_symbol()
